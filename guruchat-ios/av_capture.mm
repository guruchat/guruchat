#import "av_capture.h"

extern void onFrameCaptured(CVImageBufferRef frame);

@implementation AudioVideoCapture

@synthesize delegate;

- (void)captureSessionStoppedRunningNotification:(NSNotification *)notification
{
    NSLog(@"Capture session stopped");
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    
  
    FourCharCode format = CMFormatDescriptionGetMediaSubType(formatDescription);

    if ( connection == videoConnection ) {
        
        // Get framerate
        CMTime timestamp = CMSampleBufferGetPresentationTimeStamp( sampleBuffer );
        //[self calculateFramerateAtTimestamp:timestamp];
        
        // Get frame dimensions (for onscreen display)
        //if (self.videoDimensions.width == 0 && self.videoDimensions.height == 0)
        CMVideoDimensions videoDimensions = CMVideoFormatDescriptionGetDimensions( formatDescription );
        
        // Get buffer type
        //if ( self.videoType == 0 )
        CMVideoCodecType videoType = CMFormatDescriptionGetMediaSubType( formatDescription );
        
        CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        //CGColorSpaceRef cref = CVImageBufferGetColorSpace(pixelBuffer);
        
        
        //!!!!!!!!!!!!!!
        onFrameCaptured(pixelBuffer);
        
        
        
        //[self.delegate pixelBufferReadyForDisplay:pixelBuffer];
    }
}

- (void) setupAndStartCaptureSession
{
    if (!captureSession)
    {
        /*
         RosyWriter uses AVCaptureSession's default preset, AVCaptureSessionPresetHigh.
         */
        
        captureSession = [[AVCaptureSession alloc] init];
        
        {
            AVCaptureDevice* camera = [self videoDeviceWithPosition:
                                       //AVCaptureDevicePositionFront
                                       AVCaptureDevicePositionBack
                                       ];
            
            NSArray formats = camera.formats;
            AVCaptureDeviceFormat f;
            f.FourCharCode;
            
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            
            void (^subjectAreaDidChangeBlock)(NSNotification *) = ^(NSNotification *notification) {
                
                NSLog(@"Camera subject area did change");
                
                if( [camera lockForConfiguration:nil] )
                {
                    //                    camera.focusMode = AVCaptureFocusModeAutoFocus;
                    
                    [camera unlockForConfiguration];
                }
/*
                if (camera.focusMode == AVCaptureFocusModeLocked ){
                    // All you need to do is set the continuous focus at the center. This is the same behavior as
                    // in the stock Camera app
                    //[self continuousFocusAtPoint:CGPointMake(.5f, .5f)];
                    
                    if( [camera lockForConfiguration:nil] )
                    {
                        camera.focusMode = AVCaptureFocusModeAutoFocus;
                    
                        //    camera.focusMode = AVCaptureFocusModeContinuousAutoFocus;//AVCaptureFocusModeAutoFocus;
                   
                        CGPoint location = CGPointMake(0.5, 0.5);
                    
                        if( [camera isFocusPointOfInterestSupported] )
                            camera.focusPointOfInterest = location;
                    
                        if( [camera isExposurePointOfInterestSupported] )
                            camera.exposurePointOfInterest = location;
 
                        [camera unlockForConfiguration];
 
                    }
                } else {
                    if([camera lockForConfiguration:nil])
                    {
                        camera.focusMode = AVCaptureFocusModeLocked;

                        [camera unlockForConfiguration];
                    }
                }
*/
            };
            
            //self.subjectAreaDidChangeObserver =
            [notificationCenter addObserverForName:AVCaptureDeviceSubjectAreaDidChangeNotification
                                                                                object:nil
                                                                                 queue:nil
                                                                            usingBlock:subjectAreaDidChangeBlock];
            
            //[self addObserver:self forKeyPath:keyPathAdjustingFocus options:NSKeyValueObservingOptionNew context:NULL];
            
            AVCaptureDeviceInput *videoIn = [[AVCaptureDeviceInput alloc]
                                             initWithDevice:camera
                                             error:nil];
            
            if( [camera lockForConfiguration:nil] )
            {
                //[camera setFocusMode:AVCaptureFocusModeAutoFocus];//AVCaptureFocusModeContinuousAutoFocus];//];
                
                //camera.focusMode = AVCaptureFocusModeContinuousAutoFocus;//AVCaptureFocusModeAutoFocus;
                
                if ([camera isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                    camera.focusMode = AVCaptureFocusModeContinuousAutoFocus;//Locked;//;
                }
                
                if ([camera isSmoothAutoFocusSupported]) {
                    camera.smoothAutoFocusEnabled = YES;
                }
                
                camera.subjectAreaChangeMonitoringEnabled = YES;
                
                CGPoint location = CGPointMake(0.5, 0.5);
                
                if( [camera isFocusPointOfInterestSupported] )
                    camera.focusPointOfInterest = location;
                
                if( [camera isExposurePointOfInterestSupported] )
                    camera.exposurePointOfInterest = location;

                [camera unlockForConfiguration];
            }
            
            if ([captureSession canAddInput:videoIn])
                [captureSession addInput:videoIn];
        }
        
        {
            AVCaptureVideoDataOutput *videoOut = [[AVCaptureVideoDataOutput alloc] init];
            NSArray pixelFormats = [videoOut availableVideoCVPixelFormatTypes];
            NSNumber nativeFormat = pixelFormats.firstObject;
            
            
            
            [videoOut setAlwaysDiscardsLateVideoFrames:YES];
            [videoOut setVideoSettings:[NSDictionary
                                        dictionaryWithObject: [NSNumber numberWithInt:
                                                               kCVPixelFormatType_32BGRA
                                                               //kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
                                                               ] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
            
            {
                dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
                
                videoOut.alwaysDiscardsLateVideoFrames = YES;
                
		// TODO: Это может быть проще???
                [videoOut setSampleBufferDelegate:self queue:videoCaptureQueue];
            }
            
            if ([captureSession canAddOutput:videoOut])
                [captureSession addOutput:videoOut];
            
            videoConnection = [videoOut connectionWithMediaType:AVMediaTypeVideo];
            
            /*
             front facing camera is mounted AVCaptureVideoOrientationLandscapeLeft,
             and the back-facing camera is mounted AVCaptureVideoOrientationLandscapeRight
             */
            
            [videoConnection setVideoOrientation:
                AVCaptureVideoOrientationPortrait];
            
            //self.videoOrientation = [videoConnection videoOrientation];
        }
    }
    
    [[NSNotificationCenter defaultCenter]
        addObserver: self selector: @selector(captureSessionStoppedRunningNotification:)
        name: AVCaptureSessionDidStopRunningNotification
        object: captureSession ];
    
    if (!captureSession.isRunning)
    {
        [captureSession startRunning];
    }
}

- (AVCaptureDevice *)videoDeviceWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    
    return nil;
}


@end
