//
//  ios_camera.cpp
//  guruchat
//
//  Created by Ivan Borisenko on 10/30/14.
//  Copyright (c) 2014 guruchat.org. All rights reserved.
//

#include "./../video_capture.h"

class iOSCamera : public ICamera {
private:
    virtual void on_frame(OnCameraFrameHandler* handler, void* context)
    {
        handler_ = handler;
        context_ = context;
    }
    
    // Enable camera capture with current parameters. Dataflow may absent if camera is busy or access denied.
    virtual void on()
    {
        
    }
    
    // Disable camera capture. Dataflow interrupted immediately.
    virtual void off()
    {
        
    }
    
    OnCameraFrameHandler* handler_;
    void* context_;
};

iOSCamera backCamera;

ICamera& get_camera(const char* name)
{
    return backCamera;
    
}