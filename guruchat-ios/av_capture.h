#pragma once

#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CMBufferQueue.h>

@protocol OnFrameDelegate;

@interface AudioVideoCapture : NSObject
<AVCaptureAudioDataOutputSampleBufferDelegate,
AVCaptureVideoDataOutputSampleBufferDelegate>
{
@public
  AVCaptureConnection *videoConnection;
@private
  AVCaptureSession *captureSession;
}

- (void) setupAndStartCaptureSession;

@property (readwrite, assign) id <OnFrameDelegate> delegate;

@end

@protocol OnFrameDelegate <NSObject>
@required
- (void)pixelBufferReadyForDisplay:(CVPixelBufferRef)pixelBuffer;	// This method is always called on the main thread.
@end