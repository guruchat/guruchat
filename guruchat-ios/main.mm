#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <OpenGLES/ES1/glext.h>

#include <SystemConfiguration/SystemConfiguration.h>
#include <SystemConfiguration/SCNetworkReachability.h>
#include "Novocaine/Novocaine.h"


/*
 CGSize PhysicalPixelSizeOfScreen(UIScreen *s) {
 CGSize result = s.bounds.size;
 
 if ([s respondsToSelector: @selector(scale)]) {
 CGFloat scale = s.scale;
 result = CGSizeMake(result.width * scale, result.height * scale);
 }
 
 return result;
 }
 */

#import "av_capture.h"

@interface MyGLView : UIView <OnFrameDelegate>
{
@public
    AudioVideoCapture* avCapture;
@private
    CMBufferQueueRef previewBufferQueue;
    
    int renderBufferWidth;
    int renderBufferHeight;
    
    CVOpenGLESTextureCacheRef videoTextureCache;
    
    EAGLContext *oglContext;
    
    // OpenGL names for the renderbuffer and framebuffers used to render to this view
    //GLuint viewRenderbuffer, viewFramebuffer;
    
    // OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist)
    //GLuint depthRenderbuffer;
    
    GLuint frameBufferHandle;
    GLuint colorBufferHandle;
    GLuint passThroughProgram;
    
    BOOL displayLinkSupported;
    
    // Use of the CADisplayLink class is the preferred method for controlling your animation timing.
    // CADisplayLink will link to the main display and fire every vsync when added to a given run-loop.
    // The NSTimer class is used only as fallback when running on a pre 3.1 device where CADisplayLink
    // isn't available.
    id displayLink;
}

-(void)drawView;

@end


MyGLView *theGlView = 0;

enum {
    ATTRIB_VERTEX,
    ATTRIB_TEXTUREPOSITON,
    NUM_ATTRIBUTES
};

@implementation MyGLView


// Implement this to override the default layer class (which is [CALayer class]).
// We do this so that our view will be backed by a layer that is capable of OpenGL ES rendering.
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

/* Rotation support for UIViewController
 iOS 8+
 - (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
 [UIView setAnimationsEnabled:NO];
 
 [coordinator notifyWhenInteractionEndsUsingBlock:^(id<UIViewControllerTransitionCoordinatorContext> context) {
 [UIView setAnimationsEnabled:YES];
 }];
 
 [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
 }
 
 
 // iOS 7-
 - (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
 duration:(NSTimeInterval)duration {
 [UIView setAnimationsEnabled:NO];
 
 hidden = YES;
 [self setNeedsStatusBarAppearanceUpdate];
 }
 
 // iOS 7-
 - (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
 [UIView setAnimationsEnabled:YES];
 [UIView animateWithDuration:0.42 animations:^{
 hidden = NO;
 [self setNeedsStatusBarAppearanceUpdate];
 }];
 }
 */

// Updates the OpenGL view
- (void)drawView
{
# if 1
    // Make sure that you are drawing to the current context
    [EAGLContext setCurrentContext:oglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
    //glDisable(GL_DEPTH_TEST);
    //glDisable(GL_DITHER);
    //glEnable(GL_TEXTURE_2D);
    
    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
# if 0
    GLfloat vertices[] = {-1, -1, 0, // bottom left corner
        -1,  1, 0, // top left corner
        1,  1, 0, // top right corner
        1, -1, 0}; // bottom right corner
    
    GLubyte indices[] = {0,1,2, // first triangle (bottom left - top left - top right)
        0,2,3}; // second triangle (bottom left - top right - bottom right)
    
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indices);
# endif
    /*
     GLfloat texture[] =
     {
     0, 0,
     0, 1,
     1, 0,
     1, 1
     };
     
     GLfloat model[] =
     {
     0, 0, // lower left
     0, 0.50, // upper left
     0.50, 0, // lower right
     0.50, 0.50  // upper right
     };
     
     glEnableClientState(GL_VERTEX_ARRAY);
     glEnableClientState(GL_TEXTURE_COORD_ARRAY);
     
     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
     glVertexPointer(2, GL_FLOAT, 0, model);
     glTexCoordPointer(2, GL_FLOAT, 0, texture);
     glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
     */
    glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    [oglContext presentRenderbuffer:GL_RENDERBUFFER];
# endif
}

#define LogInfo printf
#define LogError printf

/* Compile a shader from the provided source(s) */
GLint glueCompileShader(GLenum target, GLsizei count, const GLchar **sources, GLuint *shader)
{
    GLint status;
    
    *shader = glCreateShader(target);
    glShaderSource(*shader, count, sources, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        LogInfo("Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        int i;
        
        LogError("Failed to compile shader:\n");
        for (i = 0; i < count; i++)
            LogInfo("%s", sources[i]);
    }
    
    return status;
}

/* Link a program with all currently attached shaders */
GLint glueLinkProgram(GLuint program)
{
    GLint status;
    
    glLinkProgram(program);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(program, logLength, &logLength, log);
        LogInfo("Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == 0)
        LogError("Failed to link program %d", program);
    
    return status;
}

/* Convenience wrapper that compiles, links, enumerates uniforms and attribs */
GLint glueCreateProgram(const GLchar *vertSource, const GLchar *fragSource,
                        GLsizei attribNameCt, const GLchar **attribNames,
                        const GLint *attribLocations,
                        GLsizei uniformNameCt, const GLchar **uniformNames,
                        GLint *uniformLocations,
                        GLuint *program)
{
    GLuint vertShader = 0, fragShader = 0, prog = 0, status = 1, i;
    
    // Create shader program
    prog = glCreateProgram();
    
    // Create and compile vertex shader
    status *= glueCompileShader(GL_VERTEX_SHADER, 1, &vertSource, &vertShader);
    
    // Create and compile fragment shader
    status *= glueCompileShader(GL_FRAGMENT_SHADER, 1, &fragSource, &fragShader);
    
    // Attach vertex shader to program
    glAttachShader(prog, vertShader);
    
    // Attach fragment shader to program
    glAttachShader(prog, fragShader);
    
    // Bind attribute locations
    // This needs to be done prior to linking
    for (i = 0; i < attribNameCt; i++)
    {
        if(strlen(attribNames[i]))
            glBindAttribLocation(prog, attribLocations[i], attribNames[i]);
    }
    
    // Link program
    status *= glueLinkProgram(prog);
    
    // Get locations of uniforms
    if (status)
    {
        for(i = 0; i < uniformNameCt; i++)
        {
            if(strlen(uniformNames[i]))
                uniformLocations[i] = glGetUniformLocation(prog, uniformNames[i]);
        }
        *program = prog;
    }
    
    // Release vertex and fragment shaders
    if (vertShader)
        glDeleteShader(vertShader);
    if (fragShader)
        glDeleteShader(fragShader);
    
    return status;
}

- (BOOL)initializeBuffers
{
    BOOL success = YES;
    
    glDisable(GL_DEPTH_TEST);
    
    glGenFramebuffers(1, &frameBufferHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
    
    glGenRenderbuffers(1, &colorBufferHandle);
    glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    
    CGSize sz = self.layer.frame.size;
    [oglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &renderBufferWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &renderBufferHeight);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBufferHandle);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"Failure with framebuffer generation");
        success = NO;
    }
    
    //  Create a new CVOpenGLESTexture cache
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, oglContext, NULL, &videoTextureCache);
    if (err) {
        NSLog(@"Error at CVOpenGLESTextureCacheCreate %d", err);
        success = NO;
    }
    
    // Load vertex and fragment shaders
    const GLchar *vertSrc = "attribute vec4 position;\
    attribute mediump vec4 textureCoordinate;\
    varying mediump vec2 coordinate;\
    \
    void main()\
    {\
    gl_Position = position;\
    coordinate = textureCoordinate.xy;\
    }";
    
    const GLchar *fragSrc = "varying highp vec2 coordinate;\
    uniform sampler2D videoframe;\
    \
    void main()\
    {\
    gl_FragColor = texture2D(videoframe, coordinate);\
    }";
    
    // attributes
    GLint attribLocation[NUM_ATTRIBUTES] = {
        ATTRIB_VERTEX, ATTRIB_TEXTUREPOSITON,
    };
    GLchar *attribName[NUM_ATTRIBUTES] = {
        (GLchar*)"position", (GLchar*)"textureCoordinate",
    };
    
    glueCreateProgram(vertSrc, fragSrc,
                      NUM_ATTRIBUTES, (const GLchar **)&attribName[0], attribLocation,
                      0, 0, 0, // we don't need to get uniform locations in this example
                      &passThroughProgram);
    
    if (!passThroughProgram)
        success = NO;
    
    return success;
}

- (void)pixelBufferReadyForDisplay:(CVPixelBufferRef)pixelBuffer
{
    CFRetain(pixelBuffer);
    
    dispatch_async(
        dispatch_get_main_queue(),
        ^{
            UIApplicationState currState = [UIApplication sharedApplication].applicationState;
            if (currState != UIApplicationStateBackground)
                [self displayPixelBuffer:pixelBuffer];
            CFRelease(pixelBuffer);
        }
    );
}

- (CGRect)textureSamplingRectForCroppingTextureWithAspectRatio:(CGSize)textureAspectRatio toAspectRatio:(CGSize)croppingAspectRatio
{
    CGRect normalizedSamplingRect = CGRectZero;
    CGSize cropScaleAmount = CGSizeMake(croppingAspectRatio.width / textureAspectRatio.width, croppingAspectRatio.height / textureAspectRatio.height);
    CGFloat maxScale = fmax(cropScaleAmount.width, cropScaleAmount.height);
    CGSize scaledTextureSize = CGSizeMake(textureAspectRatio.width * maxScale, textureAspectRatio.height * maxScale);
    
    if ( cropScaleAmount.height > cropScaleAmount.width ) {
        normalizedSamplingRect.size.width = croppingAspectRatio.width / scaledTextureSize.width;
        normalizedSamplingRect.size.height = 1.0;
    }
    else {
        normalizedSamplingRect.size.height = croppingAspectRatio.height / scaledTextureSize.height;
        normalizedSamplingRect.size.width = 1.0;
    }
    // Center crop
    normalizedSamplingRect.origin.x = (1.0 - normalizedSamplingRect.size.width)/2.0;
    normalizedSamplingRect.origin.y = (1.0 - normalizedSamplingRect.size.height)/2.0;
    
    return normalizedSamplingRect;
}

- (void)renderWithSquareVertices:(const GLfloat*)squareVertices textureVertices:(const GLfloat*)textureVertices
{
    // Use shader program.
    glUseProgram(passThroughProgram);
    
    // Update attribute values.
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, squareVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, 0, 0, textureVertices);
    glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
    
    // Update uniform values if there are any
    
    // Validate program before drawing. This is a good check, but only really necessary in a debug build.
    // DEBUG macro must be defined in your debug configurations if that's not already the case.
#if 0 //defined(DEBUG)
    GLint status;
    
    glValidateProgram(passThroughProgram);
    
    GLint logLength;
    glGetProgramiv(passThroughProgram, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(passThroughProgram, logLength, &logLength, log);
        //LogInfo("Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(passThroughProgram, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        //LogError("Failed to validate program %d", program);
        return;
    }
#endif
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // Present
    glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    [oglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)displayPixelBuffer:(CVImageBufferRef)pixelBuffer
{
    if (frameBufferHandle == 0) {
        BOOL success = [self initializeBuffers];
        if ( !success ) {
            NSLog(@"Problem initializing OpenGL buffers.");
        }
    }
    
    if (videoTextureCache == NULL)
        return;
    
    // Create a CVOpenGLESTexture from the CVImageBuffer
    size_t frameWidth = CVPixelBufferGetWidth(pixelBuffer);
    size_t frameHeight = CVPixelBufferGetHeight(pixelBuffer);
    CVOpenGLESTextureRef texture = NULL;
    /*
    CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, textureCache, pixelBuffer, NULL,
                                                 GL_TEXTURE_2D,
                                                 GL_RGB_422_APPLE, width, height,
                                                 GL_RGB_422_APPLE, GL_UNSIGNED_SHORT_8_8_APPLE,
                                                 1, &outTexture);*/
    
    CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                videoTextureCache,
                                                                pixelBuffer,
                                                                NULL,
                                                                GL_TEXTURE_2D,
                                                                GL_RGBA, // store and use as
                                                                frameWidth,
                                                                frameHeight,
                                                                GL_BGRA, // convert from
                                                                GL_UNSIGNED_BYTE,
                                                                0,
                                                                &texture);
    
    
    if (!texture || err) {
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage failed (error: %d)", err);
        return;
    }
    
    glBindTexture(CVOpenGLESTextureGetTarget(texture), CVOpenGLESTextureGetName(texture));
    
    // Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
    
    // Set the view port to the entire view
    glViewport(0, 0, renderBufferWidth, renderBufferHeight);
    
    static const GLfloat squareVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f,  1.0f,
    };
    
    // The texture vertices are set up such that we flip the texture vertically.
    // This is so that our top left origin buffers match OpenGL's bottom left texture coordinate system.
    const CGRect textureSamplingRect = [self textureSamplingRectForCroppingTextureWithAspectRatio:CGSizeMake(frameWidth, frameHeight) toAspectRatio:self.bounds.size];
    GLfloat textureVertices[] = {
        (GLfloat)CGRectGetMinX(textureSamplingRect), (GLfloat)CGRectGetMaxY(textureSamplingRect),
        (GLfloat)CGRectGetMaxX(textureSamplingRect), (GLfloat)CGRectGetMaxY(textureSamplingRect),
        (GLfloat)CGRectGetMinX(textureSamplingRect), (GLfloat)CGRectGetMinY(textureSamplingRect),
        (GLfloat)CGRectGetMaxX(textureSamplingRect), (GLfloat)CGRectGetMinY(textureSamplingRect),
    };
    
    // Draw the texture on the screen with OpenGL ES 2
    [self renderWithSquareVertices:squareVertices textureVertices:textureVertices];
    
    glBindTexture(CVOpenGLESTextureGetTarget(texture), 0);
    
    // Flush the CVOpenGLESTexture cache and release the texture
    CVOpenGLESTextureCacheFlush(videoTextureCache, 0);
    CFRelease(texture);
}

- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    return;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            /* start special animation */
            [avCapture->videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            [avCapture->videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
            break;
            
        case UIDeviceOrientationLandscapeRight:
            [avCapture->videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            /* start special animation */
            break;
            
        default:
            break;
    };
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        
        OSStatus err = CMBufferQueueCreate(kCFAllocatorDefault, 1,
                                           CMBufferQueueGetCallbacksForUnsortedSampleBuffers(),
                                           & previewBufferQueue);
        
        avCapture = [[AudioVideoCapture alloc] init];
        avCapture.delegate = self;
        
        [avCapture setupAndStartCaptureSession];
        
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        oglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        
        if (!oglContext || ![EAGLContext setCurrentContext:oglContext]) {
            return nil;
        }
        
        displayLinkSupported = FALSE;
        displayLink = nil;
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(orientationChanged:)
         name:UIDeviceOrientationDidChangeNotification
         object:[UIDevice currentDevice]];
        
        
        /*
         // A system version of 3.1 or greater is required to use CADisplayLink. The NSTimer
         // class is used as fallback when it isn't available.
         NSString *reqSysVer = @"3.1";
         NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
         if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
         displayLinkSupported = TRUE;
         
         glGenRenderbuffers(1, &viewRenderbuffer);
         glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
         
         // Allocate surface memory
         [oglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
         
         glGenFramebuffers(1, &viewFramebuffer);
         glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
         glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, viewRenderbuffer);
         */
        //[self setupView];
    }
    
    return self;
}

-(void)setupView
{
    const GLfloat           lightAmbient[] = {0.2, 0.2, 0.2, 1.0};
    const GLfloat           lightDiffuse[] = {1.0, 0.6, 0.0, 1.0};
    const GLfloat           matAmbient[] = {0.6, 0.6, 0.6, 1.0};
    const GLfloat           matDiffuse[] = {1.0, 1.0, 1.0, 1.0};
    const GLfloat           matSpecular[] = {1.0, 1.0, 1.0, 1.0};
    const GLfloat           lightPosition[] = {0.0, 0.0, 1.0, 0.0};
    const GLfloat           lightShininess = 100.0,
    zNear = 0.1,
    zFar = 1000.0,
    fieldOfView = 60.0;
    GLfloat                 size;
    
    //Configure OpenGL lighting
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, matAmbient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, matDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, lightShininess);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    
    //Configure OpenGL arrays
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    //glVertexPointer(3 ,GL_FLOAT, 0, teapot_vertices);
    //glNormalPointer(GL_FLOAT, 0, teapot_normals);
    glEnable(GL_NORMALIZE);
    
    //Set the OpenGL projection matrix
    glMatrixMode(GL_PROJECTION);
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)
    size = zNear * tanf(DEGREES_TO_RADIANS(fieldOfView) / 2.0);
    CGRect rect = self.bounds;
    glFrustumf(-size, size, -size / (rect.size.width / rect.size.height), size / (rect.size.width / rect.size.height), zNear, zFar);
    glViewport(0, 0, rect.size.width, rect.size.height);
    
    //Make the OpenGL modelview matrix the default
    glMatrixMode(GL_MODELVIEW);
}

- (void)dealloc
{
    
    if([EAGLContext currentContext] == oglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
}

@end

void onFrameCaptured(CVImageBufferRef frame)
{
    [theGlView pixelBufferReadyForDisplay:frame];
}

@interface guruchatAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MyGLView *glView;
    UIViewController * viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MyGLView *glView;

@end

@implementation guruchatAppDelegate

@synthesize window;
@synthesize glView;

// First code entry
- (BOOL) application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    Novocaine *audioManager = [Novocaine audioManager];
    [audioManager setInputBlock:^(float *newAudio, UInt32 numSamples, UInt32 numChannels) {
        int a = 1;
        // Now you're getting audio from the microphone every 20 milliseconds or so. How's that for easy?
        // Audio comes in interleaved, so,
        // if numChannels = 2, newAudio[0] is channel 1, newAudio[1] is channel 2, newAudio[2] is channel 1, etc.
    }];
    [audioManager play];
    
    UIApplicationState state = [application applicationState];
    if (UIApplicationStateInactive == state)
    {
        NSLog(@"App loaded into foreground");
    }
    else if(UIApplicationStateBackground == state)
    {
        NSLog(@"App loaded into background");
    }
    else
    {
        NSLog(@"App loaded with unexpectable state");
    }
    
    NSURL* url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
    
    NSLog(@"%@", url);
    
    //SCNetworkReachabilityRef =
    
    //SCNetworkReachabilitySetCallback(<#SCNetworkReachabilityRef target#>, SCNetworkReachabilityCallBack callout, <#SCNetworkReachabilityContext *context#>)
    
    //SCNetworkReachabilitySetDispatchQueue(SCNetworkReachabilityRef target, dispatch_queue_t queue);
    
    CGRect  viewRect = [[UIScreen mainScreen] bounds];
    
    UIScreen* mainScreen = [UIScreen mainScreen];
    UIScreenMode* screenMode = [mainScreen currentMode];
    viewRect.size = [screenMode size];
    
    //viewRect.size.height = 1136;//568;
    
    window = [[UIWindow alloc] initWithFrame:viewRect];
    viewController = [[UIViewController alloc] init];
    
    glView = [[MyGLView alloc] initWithFrame:viewRect];
    theGlView = glView;
    [glView initializeBuffers];
    glView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    viewController.view = glView;
    
    [glView setFrame:viewRect];
    window.rootViewController = viewController;
    // Add the view controller's view to the window and display.
    [window addSubview:window.rootViewController.view];
    [window makeKeyAndVisible];
    //[glView->avCapture setupAndStartCaptureSession];
    
    return YES;
}

- (BOOL) application:(UIApplication*)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"Open url %@", url);
    return YES;
}

// Final initialization before first display
- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"App state loaded when started");
    
    //CGRect  viewRect = CGRectMake(0, 0, 100, 100);
    //UIView* myView = [[UIView alloc] initWithFrame:viewRect];
    
    //[glView startAnimation];
    return YES;
}

// Last-minute preparation before foreground (Alt+Tab to our app or first run).
- (void) applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"Activated (focused/restored/maximized/visible)");
    ///[glView drawView];
    //[glView startAnimation];
}

- (void) applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"Runs in foreground (wake up/activate complex resources/recreate buffers/activate cache creation/continu worker threads)");
}


// Focus lost (Alt+Tab to another app)
- (void) applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"Deactivated (unfocused/minimized/hidden)");
    //[glView stopAnimation];
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"Runs in background (sleep/deactivate complex resources/flush cache/free large memory buffers/pause threads)");
    [glView drawView];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"willTerminate");
    //[glView stopAnimation];
}



@end




int main(int argc, char *argv[]) {
    int retVal = 0;
    
    @autoreleasepool {
        retVal = UIApplicationMain(argc, argv,
                                   nil,
                                   NSStringFromClass([guruchatAppDelegate class]));
    }
    NSLog(@"main() exits now.");
    return retVal;
}
