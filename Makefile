.PHONY: clean All

All:
	@echo "----------Building project:[ guruchat_server - Debug ]----------"
	@cd "guruchat_server" && $(MAKE) -f  "guruchat_server.mk"
clean:
	@echo "----------Cleaning project:[ guruchat_server - Debug ]----------"
	@cd "guruchat_server" && $(MAKE) -f  "guruchat_server.mk" clean
