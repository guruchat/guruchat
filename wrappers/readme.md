Platform-dependent code for different OSes. All code should be included in the application project for corresponding OS.
The code and headers are all internal. Access to it by use of headers in ./../wrapi/platform. For correct compilation
client code should implement corresponding callback interfaces in ./../wrapi/client.
