#include <UIKit/UIScreen.h>
#include <QuartzCore/CADisplayLink.h>

@interface VSyncCallback : NSObject {
    void (*_callback)();
}
@end

@implementation VSyncCallback
- (id) initWithCallback: (void (*)()) callback
{
    self = [super init];
    if (self) {
        _callback = callback;
    }
    return self;
}

- (void)onScreenVSync:(CADisplayLink *)displayLink {
    [displayLink invalidate];
    _callback();
    [self autorelease];
}
@end

void wrapi_request_vsync(void (*onVSync)()) {
    CADisplayLink* dl = [[UIScreen mainScreen]
                         displayLinkWithTarget:[[VSyncCallback alloc] initWithCallback:onVSync]
                         selector:@selector(onScreenVSync:)];
    [dl setFrameInterval:60];
    [dl addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}
