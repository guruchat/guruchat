#include "./../../../wrapi/client/application.h"
#include "./../../../wrapi/platform/application.h"

extern "C" {
#include "application.h"
}

namespace wrapi {

    
class ApplicationWrapper : public IApplicationLoadSubsystem {
public:
    ApplicationWrapper(ELoadReason loadReason) :
        loadReason_(loadReason),
        onExitHandler_(0) {
        
    }
    
    void onExitApplication() {
        if (onExitHandler_) {
            onExitHandler_();
            onExitHandler_ = 0;
        }
    }
    
private:
    
    virtual ELoadReason getLoadReason() {
        return loadReason_;
    }
    
    virtual void subscribeForExitEvent(FnType_onExitHandler* onExitHandler) {
        onExitHandler_ = onExitHandler;
    }
    
    virtual void log(const char16_t* message) {
        wrapi_ios_log_string(reinterpret_cast<const unsigned short*>(message));
    }
    
    virtual class ITimeServices* getTimeServices(void) {
        return 0;
    }

    ELoadReason loadReason_;
    FnType_onExitHandler* onExitHandler_;
};
    
ApplicationWrapper* g_pApp = 0;
/*
extern "C" int wrapi_notify_client_application_loaded(ELoadReason loadReason) {
    static ApplicationWrapper g_app(static_cast<IApplicationLoadSubsystem::ELoadReason>(loadReason));
    g_pApp = &g_app;

    return on_application_started(&g_app) ? 0 : -1;
}

extern "C" void wrapi_notify_client_application_terminate() {
    g_pApp->onExitApplication();
}*/
}