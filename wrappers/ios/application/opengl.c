#import <OpenGLES/ES2/gl.h>
#include <string.h>

/* Compile a shader from the provided source(s) */
GLint glueCompileShader(GLenum target, GLsizei count, const GLchar **sources, GLuint *shader) {
    *shader = glCreateShader(target);
    glShaderSource(*shader, count, sources, NULL);
    glCompileShader(*shader);
    
    //GLint status;
    //glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);

    return 1;//status;
}

/* Link a program with all currently attached shaders */
GLint glueLinkProgram(GLuint program) {
    glLinkProgram(program);

    //GLint status;
    //glGetProgramiv(program, GL_LINK_STATUS, &status);

    return 1;//status;
}

/* Convenience wrapper that compiles, links, enumerates uniforms and attribs */
GLint glueCreateProgram(const GLchar *vertSource, const GLchar *fragSource,
                        GLsizei attribNameCt, const GLchar **attribNames,
                        const GLint *attribLocations,
                        GLsizei uniformNameCt, const GLchar **uniformNames,
                        GLint *uniformLocations,
                        GLuint *program) {
    GLuint vertShader = 0, fragShader = 0, prog = 0, status = 1, i;
    
    // Create shader program
    prog = glCreateProgram();
    
    // Create and compile vertex shader
    //status *=
    glueCompileShader(GL_VERTEX_SHADER, 1, &vertSource, &vertShader);
    
    // Create and compile fragment shader
    //status *=
    glueCompileShader(GL_FRAGMENT_SHADER, 1, &fragSource, &fragShader);
    
    // Attach vertex shader to program
    glAttachShader(prog, vertShader);
    
    // Attach fragment shader to program
    glAttachShader(prog, fragShader);
    
    // Bind attribute locations
    // This needs to be done prior to linking
    for (i = 0; i < attribNameCt; i++)
    {
        if(strlen(attribNames[i]))
            glBindAttribLocation(prog, attribLocations[i], attribNames[i]);
    }
    
    // Link program
    //status *=
    glueLinkProgram(prog);
    
    // Get locations of uniforms
    //if (status)
    //{
    /*
        for(i = 0; i < uniformNameCt; i++)
        {
            if(strlen(uniformNames[i]))
                uniformLocations[i] = glGetUniformLocation(prog, uniformNames[i]);
        }
     */
        *program = prog;
    //}
    
    // Release vertex and fragment shaders
    if (vertShader)
        glDeleteShader(vertShader);
    if (fragShader)
        glDeleteShader(fragShader);
    
    return status;
}
