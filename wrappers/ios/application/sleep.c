#include <ctype.h>
#include <mach/mach_time.h>

static uint64_t nanos_to_abs(uint64_t nanos) {
    mach_timebase_info_data_t timebase_info;
    mach_timebase_info(&timebase_info);
    
    return nanos * timebase_info.denom / timebase_info.numer;
}

void wrapi_sleep(unsigned long long nanos)
{
    uint64_t time_to_wait = nanos_to_abs(nanos);

    uint64_t now = mach_absolute_time();
    mach_wait_until(now + time_to_wait);
}
