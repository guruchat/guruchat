#pragma once

// Load reason
typedef enum _ELoadReason {
    LOADREASON_UNDEFINED = 2,
    // Open application
    LOADREASON_USER,
    // Run background service / remote first-load activation
    LOADREASON_AUTO,
    // Url or associated file format handler activation
    LOADREASON_URL
} ELoadReason;

//int wrapi_notify_client_application_loaded(ELoadReason loadReason);

void wrapi_notify_client_application_terminate();

void wrapi_ios_log_string(const unsigned short* message);




// First entry point. Called back at most first time.
int on_application_loaded();

// Called on first activation
void on_main_window_created();

int on_url_activation(const char* url);

void on_application_exit();