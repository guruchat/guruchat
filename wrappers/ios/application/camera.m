//#include <UIKit/UIApplication.h>
@import AVFoundation;

@interface CameraCaptureDelegate : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate> {
}
@end

@implementation CameraCaptureDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    FourCharCode fcc = CMFormatDescriptionGetMediaSubType(formatDescription);
    char* fccInv = (char*)&fcc;
    char fccFormat[5] = { fccInv[3], fccInv[2], fccInv[1], fccInv[0], 0 };
    
    if (fccFormat[3] == 'v') {
        // v -> kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
        // Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).
        // f -> kCVPixelFormatType_420YpCbCr8BiPlanarFullRange
        // Bi-Planar Component Y'CbCr 8-bit 4:2:0, full-range (luma=[0,255] chroma=[1,255]).
        return;
    }
    
    CMVideoDimensions videoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription);
    
    // NSLog(@"%s %dx%d", fccFormat, videoDimensions.width, videoDimensions.height);
    printf(".");
    
}

- (void)captureSessionStoppedRunningNotification:(NSNotification *)notification
{
    AudioServicesPlaySystemSound(1003);
    NSLog(@"Capture session stopped");
}

- (void)captureSessionWasInterruptedNotification:(NSNotification *)notification
{
    AudioServicesPlaySystemSound(1003);
    NSLog(@"Capture session interrupted");
}

- (void)enumAndCaptureCameras
{
    NSArray* cameras = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice* frontCamera = nil;
    AVCaptureDevice* backCamera = nil;
    AVCaptureDeviceFormat* frontFormat = nil;
    AVCaptureDeviceFormat* backFormat = nil;
    for (AVCaptureDevice* camera in cameras) {
        // frontal/back/external
        NSInteger position = camera.position;
        
        if (position == AVCaptureDevicePositionBack) {
            backCamera = camera;
        } else if (position == AVCaptureDevicePositionFront) {
            frontCamera = camera;
        }
        
        NSArray* formats = camera.formats;
        for (AVCaptureDeviceFormat* format in formats) {
            CMFormatDescriptionRef formatDescription = format.formatDescription;
            FourCharCode fcc = CMFormatDescriptionGetMediaSubType(formatDescription);
            char* fccInv = (char*)&fcc;
            char fccFormat[5] = { fccInv[3], fccInv[2], fccInv[1], fccInv[0], 0 };
            
            if (fccFormat[3] == 'v') {
                // v -> kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
                // Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).
                // f -> kCVPixelFormatType_420YpCbCr8BiPlanarFullRange
                // Bi-Planar Component Y'CbCr 8-bit 4:2:0, full-range (luma=[0,255] chroma=[1,255]).
                continue;
            }
            
            CMVideoDimensions videoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription);
            float fov = format.videoFieldOfView;
            BOOL binned = format.videoBinned;
            
            NSArray* frameRates = format.videoSupportedFrameRateRanges;
            
            if (position == AVCaptureDevicePositionBack
                && videoDimensions.width == 3264
                && videoDimensions.height == 2448) {
                backFormat = format;
            } else if (position == AVCaptureDevicePositionFront
                       && videoDimensions.width == 1280
                       && videoDimensions.height == 960) {
                frontFormat = format;
            }
            
            for (AVFrameRateRange* frameRate in frameRates) {
                NSLog(@"%s %dx%d%s fov%d %d..%dfps", fccFormat,
                      videoDimensions.width, videoDimensions.height,
                      binned ? "bin" : "norm",
                      (int)fov,
                      (int)frameRate.minFrameRate, (int)frameRate.maxFrameRate);
            }
        }
    }
    
    frontCamera.modelID;
    frontCamera.localizedName;
    frontCamera.uniqueID;
    
    
    AVCaptureVideoDataOutput* videoOut = [[AVCaptureVideoDataOutput alloc] init];
    videoOut.videoSettings = [NSDictionary
                              dictionaryWithObject:
                              [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange]
                              forKey:
                              (id)kCVPixelBufferPixelFormatTypeKey];
    
    videoOut.alwaysDiscardsLateVideoFrames = YES;
    
    dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
    [videoOut setSampleBufferDelegate:self queue:videoCaptureQueue];
    
    
    AVCaptureSession* captureSessionFront = [[AVCaptureSession alloc] init];
    
    // AVCaptureSessionWasInterruptedNotification
    AVCaptureDeviceInput* videoInFront = [[AVCaptureDeviceInput alloc] initWithDevice:frontCamera error:nil];
    
    if ([captureSessionFront canAddInput:videoInFront]
        && [captureSessionFront canAddOutput:videoOut]) {
        [captureSessionFront addInput:videoInFront];
        [captureSessionFront addOutput:videoOut];
        
        for (AVCaptureConnection* connection in videoOut.connections) {
            // connection.output;
            AVCaptureVideoOrientation orientation = connection.videoOrientation;
        }
        
        // AVCaptureConnection* videoConnection = [videoOut connectionWithMediaType:AVMediaTypeVideo];
        
        AVCaptureConnection* connection = videoOut.connections.firstObject;
        
        
        // front facing camera is mounted AVCaptureVideoOrientationLandscapeLeft,
        // and the back-facing camera is mounted AVCaptureVideoOrientationLandscapeRight
        
        
        //videoConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
        
        if ([frontCamera lockForConfiguration:nil])
        {
            frontCamera.activeFormat = frontFormat;
            [frontCamera unlockForConfiguration];
        }
        //[captureSessionFront ]
        //[captureSessionFront startRunning];
    }
    
    AVCaptureVideoDataOutput* videoOut2 = [[AVCaptureVideoDataOutput alloc] init];
    videoOut2.videoSettings = [NSDictionary
                               dictionaryWithObject:
                               [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange]
                               forKey:
                               (id)kCVPixelBufferPixelFormatTypeKey];
    videoOut2.alwaysDiscardsLateVideoFrames = YES;
    
    dispatch_queue_t videoCaptureQueue2 = dispatch_queue_create("Video Capture Queue 2", DISPATCH_QUEUE_SERIAL);
    [videoOut2 setSampleBufferDelegate:self queue:videoCaptureQueue2];
    
    AVCaptureSession* captureSessionBack = [[AVCaptureSession alloc] init];
    
    
    AVCaptureDeviceInput* videoInBack = [[AVCaptureDeviceInput alloc] initWithDevice:backCamera error:nil];
    if ([captureSessionBack canAddInput:videoInBack]
        && [captureSessionBack canAddOutput:videoOut2]) {
        [captureSessionBack addInput:videoInBack];
        [captureSessionBack addOutput:videoOut2];
        
        for (AVCaptureConnection* connection in videoOut2.connections) {
            // connection.output;
            AVCaptureVideoOrientation orientation = connection.videoOrientation;
        }
        
        if ([backCamera lockForConfiguration:nil])
        {
            backCamera.activeFormat = backFormat;
            [backCamera unlockForConfiguration];
        }
        
        [[NSNotificationCenter defaultCenter]
         addObserver: self
         selector: @selector(captureSessionStoppedRunningNotification:)
         name: AVCaptureSessionDidStopRunningNotification
         object: captureSessionBack ];
        
        [[NSNotificationCenter defaultCenter]
         addObserver: self
         selector: @selector(captureSessionWasInterruptedNotification:)
         name: AVCaptureSessionWasInterruptedNotification
         object: captureSessionBack ];
        
        
        
        //NSLog(@"Back start...");
        [captureSessionBack startRunning];
        //NSLog(@"Back started.");
    }
    /*
     double delayInSeconds = 35.0;
     dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
     dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     //NSLog(@"Front start...");
     //[captureSessionFront startRunning];
     //NSLog(@"Front started.");
     });
     
     delayInSeconds = 45.0;
     popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
     dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     //NSLog(@"Back start...");
     //[captureSessionBack startRunning];
     //NSLog(@"Back started.");
     });
     */
}

@end
