
void wrapi_set_system_ui_visibility(bool visible);

void wrapi_subscribe_system_ui_visibility();

// Set custom system ui orientation.
void wrapi_set_system_ui_orientation();

// Don't tell OpenGL renderer that it should rotate!!!
// void wrapi_set_orientation(); - this function should not be exist!
//   Rationale: rotation of camera video frame could be rendered as moving of its corner points to another place,
//   actually, without ANY rotation! With beautiful SIZE ACCOMODATION effect instead of reload of all vertices.
//   Also, we could easily rotate texture coords when it needed.
// UI should be designed to work with it in active, dynamic conditions - non-normal orientation,
// shakes, handling with a hand, etc.

// iPhone 5 locks screen routation with a weird label 'Portrait Orientation Lock' On/Off,
// so we couldn't use it for our orientation lock. The lock comes to us with Portrait Orientation always.
// Also, we start with the weird Portrait Orientation.
// So, our UI will support orientation changed notifications only when we allow it to.
// We could do this with an internal (Accelerometer-based) orientation lock release / hold.
void wrapi_subscribe_system_ui_orientation();

// Accelerator-based orientation to support free / lock custom orientations.
// Filtered specially to support orientation hints only.
void wrapi_subscribe_real_orientation();

// We woun't implement customization code like void wrapi_lock_system_ui_orientation(bool lock);
// the implementation should be on the client side based on real orientation notfications and set orientation
// API call.

