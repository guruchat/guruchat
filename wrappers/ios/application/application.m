#include "application.h"
#include "thread.h"
#include "opengl.h"

#include <UIKit/UIApplication.h>
#import <CoreMotion/CoreMotion.h>

#include <UIKit/UIScreen.h>
#include <UIKit/UIScreenMode.h>
#include <UIKit/UIWindow.h>
#include <UIKit/UIViewController.h>
#include <UIKit/UITouch.h>

#include <QuartzCore/CAEAGLLayer.h>
#include <UIKit/UIImage.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

#include <AudioToolbox/AudioServices.h>
#include <wchar.h>

@interface WrapiAppDelegate : NSObject <UIApplicationDelegate> {
}

@property (nonatomic, retain) IBOutlet UIWindow* window;

@end

int deviceOrientationFromAcceleration(CGFloat x, CGFloat y) {
    // Get the current device angle
    float xx = -x;
    float yy = y;
    float angle = atan2(yy, xx);
    
    if (angle >= -2.25f && angle <= -0.75f) {
        return UIInterfaceOrientationPortrait;
    }
    else if (angle >= -0.75f && angle <= 0.75f) {
        return UIInterfaceOrientationLandscapeRight;
    }
    else if (angle >= 0.75f && angle <= 2.25f) {
        return UIInterfaceOrientationPortraitUpsideDown;
    }
    else if (angle <= -2.25f || angle >= 2.25f) {
        return UIInterfaceOrientationLandscapeLeft;
    }
    
    return 0;
}


@interface MyOpenGLLayer : CAEAGLLayer {
}
@end

@implementation MyOpenGLLayer

+ (instancetype) layer {
    return [[MyOpenGLLayer alloc] init];//[CAEAGLLayer class];
}

- (instancetype) init {
    self = [super init];
    self.doubleSided = NO;
    self.opaque = YES;
    return self;
}

- (id)initWithLayer:(id)layer {
    if ((self = [super initWithLayer:layer])) {
        if ([layer isKindOfClass:[MyOpenGLLayer class]]) {
            // Copy custom property values between layers
            MyOpenGLLayer *other = (MyOpenGLLayer *)layer;
            
            //self.currentProgress = other.currentProgress;
        }
    }
    return self;
}

+ (BOOL)needsDisplayForKey:(NSString *)key {
    // To force animation when our custom properties change
    BOOL result;
    if ([key isEqualToString:@"currentProgress"]) {
        result = YES;
    }
    else {
        result = [super needsDisplayForKey:key];
    }
    return result;
}

// Called from super.initWithFrame too!
- (void) setFrame:(CGRect)frame
{
    //frame.origin.y = 0;
    //frame.size.height = 568;
    // Call the parent class to move the view
    [super setFrame:frame];
    
    // Do your custom code here.
}

- (void) setContentsScale:(CGFloat)contentsScale {
    NSLog(@"MyOpenGLLayer::setConentsScale:%f", contentsScale);
    super.contentsScale = contentsScale;
}

- (void)drawInContext:(CGContextRef)c {
    // Custom layer drawing
}


- (void)display {
    id contents = self.contents;
    [super display];
}


- (void)displayIfNeeded {
    [super displayIfNeeded];
}

- (void)layoutIfNeeded {
    [super layoutIfNeeded];
}
/*
- (void)addAnimation:(CAAnimation *)anim forKey:(NSString *)key {
    
}*/

/* Called when the layer requires layout. The default implementation
 * calls the layout manager if one exists and it implements the
 * -layoutSublayersOfLayer: method. Subclasses can override this to
 * provide their own layout algorithm, which should set the frame of
 * each sublayer. */
// Direct override here
- (void)layoutSublayers {
    CGRect thisFrame = self.frame;
    CGRect bounds = self.bounds;
    CATransform3D transform = self.transform;

    CALayer* superlayer = self.superlayer;
    CALayerArray* sublayers = self.sublayers;
    
    NSLog(@"MyOpenGLLayer::layoutSublayers");
    printf("(was %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f)n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    
    [super layoutSublayers];
    
    thisFrame = self.frame;
    bounds = self.bounds;
    transform = self.transform;
    
    NSLog(@"MyOpenGLLayer::layoutSublayers");
    printf("(become %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f)\n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
}

@end








@interface MyGLView : UIWindow // Window
{
@private
    GLuint frameBufferHandle;
    GLuint colorBufferHandle;
    GLuint passThroughProgram;
    
    EAGLContext* oglContext;
    
    int renderBufferWidth;
    int renderBufferHeight;
    
    GLuint _textureId;
    
    // TODO: сделать стандартный OpenGL
    //CVOpenGLESTextureCacheRef videoTextureCache;
}
@end








@implementation MyGLView

+ (Class) layerClass
{
    return [MyOpenGLLayer class];//[CAEAGLLayer class];
}


//---------- START of CALayerDelegate Informal Protocol ---------------

- (void)displayLayer:(CALayer *)layer {
    // [super displayLayer:layer];
    
    NSLog(@"MyGLView::displayLayer: -> manual drawView");
    
    [self drawView:NO];
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    [super drawLayer:layer inContext:ctx];
}

// If you use layoutManager (only 7.0+), comment this!!!
- (void)layoutSublayersOfLayer:(CALayer *)layer {
    CGRect thisFrame = self.frame;
    CGRect bounds = self.bounds;
    CGPoint center = self.center;
    CGAffineTransform transform = self.transform;
    
    UIView* superview = self.superview;
    
    NSLog(@"MyGLView::layoutSublayersOfLayer");
    printf("(was %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);
    
    //NSLayoutManager* layoutManager = layer.layoutManager;
    [super layoutSublayersOfLayer:layer];
    
    thisFrame = self.frame;
    bounds = self.bounds;
    transform = self.transform;
    center = self.center;
    
    NSLog(@"MyGLView::layoutSublayersOfLayer");
    printf("(become %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);
}


// - (id<CAAction>)actionForLayer:(CALayer *)layer forKey:(NSString *)key

//---------- END of CALayerDelegate Informal Protocol ---------------


- (void)layoutSubviews {
    CGRect thisFrame = self.frame;
    CGRect bounds = self.bounds;
    CGPoint center = self.center;
    CGAffineTransform transform = self.transform;
    
    UIView* superview = self.superview;
    
    NSLog(@"MyGLView::layoutSubviews");
    printf("(was %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);

    [super layoutSubviews];
    
    thisFrame = self.frame;
    bounds = self.bounds;
    transform = self.transform;
    center = self.center;
    
    NSLog(@"MyGLView::layoutSubviews");
    printf("(become %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);
}

// Called when some property enumerated by layer's needsDisplayForKey is changed.
- (void)setNeedsDisplay {
    [super setNeedsDisplay];
}

- (void)setHidden:(BOOL)hidden {
    if (hidden) {
        NSLog(@"MyGLView::setHidden:TRUE");
    } else {
        NSLog(@"MyGLView::setHidden:FALSE");
    }
    [super setHidden:hidden];
    if (oglContext && !hidden) {
        NSLog(@"MyGLView::setHidden:FALSE -> calling displayLayer:");
        [self displayLayer:self.layer];
    }
}

- (void)setNeedsDisplayInRect:(CGRect)rect {
    [super setNeedsDisplayInRect:rect];
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.multipleTouchEnabled = YES;
        
        CAEAGLLayer* eaglLayer = (CAEAGLLayer *)self.layer;
        eaglLayer.opaque = YES;

        UIScreen* mainScreen = [UIScreen mainScreen];
        CGFloat scale = mainScreen.scale;
        // eaglLayer.contentsScale = scale;

        oglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        
        if (!oglContext || ![EAGLContext setCurrentContext:oglContext]) {
            return nil;
        }
        
        EAGLSharegroup* theOpenGl = oglContext.sharegroup;
        EAGLContext* openGlES1 = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1 sharegroup:theOpenGl];
        // Release current context:
        // [EAGLContext setCurrentContext:nil];
        
        [self initializeBuffers];
    }
    
    return self;
}

- (void) setContentScaleFactor:(CGFloat)contentsScale {
    NSLog(@"MyGLView::setContentScaleFactor:%f", contentsScale);
    [super setContentScaleFactor:contentsScale];
}
/*
- (void)addMotionEffect:(UIMotionEffect *)effect {
    [super addMotionEffect:effect];
}
*/
- (void)drawRect:(CGRect)rect {
    return [super drawRect:rect];
}

- (void)setTransform:(CGAffineTransform)transform {
    [super setTransform:transform];
}

// Called from super.initWithFrame too!
- (void) setFrame:(CGRect)frame
{
    //frame.origin.y = 0;
    //frame.size.height = 568;
    // Call the parent class to move the view
    
    CGRect thisFrame = self.frame;
    CGRect bounds = self.bounds;
    CGPoint center = self.center;
    CGAffineTransform transform = self.transform;
    
    UIView* superview = self.superview;
    
    NSLog(@"MyGLView::setFrame:%f:%f:%f:%f",
        frame.origin.x, frame.origin.y,
        frame.size.width, frame.size.height);
    printf("(was %f:%f:%f:%f; \n",
          thisFrame.origin.x, thisFrame.origin.y,
          thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);
    
    [super setFrame:frame];
    
    center = self.center;
    thisFrame = self.frame;
    bounds = self.bounds;
    transform = self.transform;
    
    printf("(become %f:%f:%f:%f; \n",
           thisFrame.origin.x, thisFrame.origin.y,
           thisFrame.size.width, thisFrame.size.height);
    printf("bounds %f:%f:%f:%f; \n",
           bounds.origin.x, bounds.origin.y,
           bounds.size.width, bounds.size.height);
    printf("center %f:%f; \n",
           center.x, center.y);
    printf("transform %f:%f:[%f:%f:%f:%f])\n",
           transform.tx, transform.ty,
           transform.a, transform.b, transform.c, transform.d);
    /*
    [super setCenter:CGPointMake((self.frame.size.width + 2 * self.frame.origin.x)/2,
                                 (self.frame.size.height + 2 * self.frame.origin.y)/2)];
    */
}


enum {
    ATTRIB_VERTEX,
    ATTRIB_TEXTUREPOSITON,
    NUM_ATTRIBUTES
};

/*
CGRect textureSamplingRectForCroppingTextureWithAspectRatio(CGSize textureAspectRatio, CGSize croppingAspectRatio) {
    CGRect normalizedSamplingRect = CGRectZero;
    CGSize cropScaleAmount = CGSizeMake(croppingAspectRatio.width / textureAspectRatio.width, croppingAspectRatio.height / textureAspectRatio.height);
    CGFloat maxScale = fmax(cropScaleAmount.width, cropScaleAmount.height);
    CGSize scaledTextureSize = CGSizeMake(textureAspectRatio.width * maxScale, textureAspectRatio.height * maxScale);
    
    if ( cropScaleAmount.height > cropScaleAmount.width ) {
        normalizedSamplingRect.size.width = croppingAspectRatio.width / scaledTextureSize.width;
        normalizedSamplingRect.size.height = 1.0;
    }
    else {
        normalizedSamplingRect.size.height = croppingAspectRatio.height / scaledTextureSize.height;
        normalizedSamplingRect.size.width = 1.0;
    }
    // Center crop
    normalizedSamplingRect.origin.x = (1.0 - normalizedSamplingRect.size.width)/2.0;
    normalizedSamplingRect.origin.y = (1.0 - normalizedSamplingRect.size.height)/2.0;
    
    return normalizedSamplingRect;
}

 - (CVPixelBufferRef) newPixelBufferFromCGImage:(CGImageRef)image forFrameSize:(CGSize)frameSize {
 NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
 [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
 [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
 nil];
 CVPixelBufferRef pxbuffer = NULL;
 CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width,
 frameSize.height, kCVPixelFormatType_32ARGB, (CFDictionaryRef) options,
 &pxbuffer);
 NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
 
 CVPixelBufferLockBaseAddress(pxbuffer, 0);
 void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
 NSParameterAssert(pxdata != NULL);
 
 CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
 CGContextRef context = CGBitmapContextCreate(pxdata, frameSize.width,
 frameSize.height, 8, 4*frameSize.width, rgbColorSpace,
 kCGImageAlphaNoneSkipFirst);
 NSParameterAssert(context);
 CGContextConcatCTM(context, frameTransform);
 CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image),
 CGImageGetHeight(image)), image);
 CGColorSpaceRelease(rgbColorSpace);
 CGContextRelease(context);
 
 CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
 
 return pxbuffer;
 }
 */

- (void)drawView:(unsigned char*)bannerOnly
{
    UIView* v = self;//.rootViewController.view;
    
    NSLog(@"Redraw view");

    //CVImageBufferRef pixelBuffer = 0;//[self newPixelBufferFromCGImage:NULL];//CMSampleBufferGetImageBuffer(sampleBuffer);
    
    //NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
    //                         nil];
    //[NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
    //                         [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
    //                         nil];
    
    unsigned char* rawPixels = malloc(renderBufferWidth * renderBufferHeight * 4);
    /*
    CVPixelBufferCreate(kCFAllocatorDefault,
                        renderBufferWidth,
                        renderBufferHeight, kCVPixelFormatType_32BGRA, (CFDictionaryRef) options,
                        &pixelBuffer);
    */
    //CVPixelBufferLockBaseAddress( pixelBuffer, 0 );
    
    int bufferWidth = renderBufferWidth;
        //CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = renderBufferHeight;
        //CVPixelBufferGetHeight(pixelBuffer);
    
    //unsigned char* rawPixels = (unsigned char *)CVPixelBufferGetBaseAddress(pixelBuffer);
    
    unsigned char *pixel = rawPixels;
    
    //pixel[2] = 255;
    
    for( int row = 0; row < bufferHeight; row++ ) {
        for( int column = 0; column < bufferWidth; column++ ) {
            pixel[0] = 0;
            pixel[1] = 0; // De-green (second pixel in BGRA is green)
            pixel[2] = 0;//column && row ? 0 : 255;
            pixel[2] = 0;//255;
            pixel[3] = 0;//127;
            
            if (column == 0 || row == 0 || column == bufferWidth - 1 || row == bufferHeight - 1) {
                /*
                 if (column == 2 && row == 2
                 || column == 2 && row == bufferHeight - 3
                 || column == bufferWidth - 3 && row == 2
                 || column == bufferWidth - 3 && row == bufferHeight - 3
                 
                 || column == 3 && row == 3
                 || column == 3 && row == bufferHeight - 4
                 || column == bufferWidth - 4 && row == 3
                 || column == bufferWidth - 4 && row == bufferHeight - 4
                 
                 || column == bufferWidth / 2
                 || row == bufferHeight / 2) { */
                
                if (row) {
                    pixel[0] = 0;
                    pixel[1] = 0;
                    pixel[2] = 255;
                    pixel[3] = 255;
                } else {
                    pixel[0] = 255;
                    pixel[1] = 255;
                    pixel[2] = 255;
                    pixel[3] = 255;
                }
            }
            
            pixel += 4;
        }
    }
/*
    
    if (frameBufferHandle == 0) {
        BOOL success = [self initializeBuffers];
        if ( !success ) {
            NSLog(@"Problem initializing OpenGL buffers.");
        }
    }
    
    if (videoTextureCache == NULL)
        return;
  */
    
    // Create a CVOpenGLESTexture from the CVImageBuffer
    //size_t frameWidth = CVPixelBufferGetWidth(pixelBuffer);
    //size_t frameHeight = CVPixelBufferGetHeight(pixelBuffer);
    
    //[EAGLContext setCurrentContext:oglContext];
    
    GLuint textureId = self->_textureId;


    static BOOL firstCall = YES;
    
    BOOL liveTexture = glIsTexture(textureId);
    
    if (bannerOnly) {
        rawPixels = bannerOnly;
    }
    
    // target, level, internalformat,
    // width, height, border,
    // format, type, pixels
    if (firstCall || !liveTexture) {
        firstCall = NO;
        glBindTexture(GL_TEXTURE_2D, 0);
        glGenTextures(1, &textureId);
        self->_textureId = textureId;
        glBindTexture(GL_TEXTURE_2D, textureId);
        
        glTexImage2D(GL_TEXTURE_2D, 0,
                 GL_RGBA,
                 bufferWidth, bufferHeight, 0,
                 GL_RGBA,
                 
                 // Supported GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT_5_6_5
                 // GL_UNSIGNED_SHORT_4_4_4_4, and GL_UNSIGNED_SHORT_5_5_5_1
                 GL_UNSIGNED_BYTE,
                 rawPixels);
    } else {
        // target, level, xoffset, yoffset, width, height,
        // format, type, pixels
        glBindTexture(GL_TEXTURE_2D, textureId);
        glTexSubImage2D(GL_TEXTURE_2D, 0,
                        0, 0,
                        bufferWidth, bufferHeight,
                        GL_RGBA,
                        GL_UNSIGNED_BYTE,
                        rawPixels);
    }
    free(rawPixels);
    rawPixels = 0;

    //CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );

# if 0
    CVOpenGLESTextureRef texture = NULL;
    
    CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                videoTextureCache,
                                                                pixelBuffer,
                                                                NULL,
                                                                GL_TEXTURE_2D,
                                                                GL_RGBA, // store and use as
                                                                frameWidth,
                                                                frameHeight,
                                                                GL_BGRA, // convert from
                                                                GL_UNSIGNED_BYTE,
                                                                0,
                                                                &texture);
    
    if (!texture || err) {
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage failed (error: %d)", err);
        return;
    }
    
    //textureId = CVOpenGLESTextureGetName(texture);
    
# endif
    //CFRelease(pixelBuffer);
    
    //GLuint textureTargetId = 0;
    
    //textureTargetId = GL_TEXTURE_2D;//CVOpenGLESTextureGetTarget(texture);

    // LIZA's ORANGE:
    //glClearColor(0.945f, 0.506f, 0.212f, 1.0f);
    // DIMA's RED
    glClearColor(255.0f / 255.0f, 133.0f / 255.0f, 135.0f / 255.0f, 1.0f);
    if (bannerOnly ) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }
    glClear(GL_COLOR_BUFFER_BIT);
    
    //glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
    //glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    //static firstClear = false;
    //if (!firstClear) {
    //   glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    //    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //}
    
    // Set the view port to the entire view
    glViewport(0, 0, renderBufferWidth, renderBufferHeight);
    
    int left = 0;//!firstClear ? 1 : 22;
    int top = 0;//43;
    
    int w = renderBufferWidth;//640;
    int h = renderBufferHeight;//640;
    
    // y flipped:
    // -1 becomes +1
    // +1 becomes -1
    // 0 -> +1       glY = scrY / scrH * 2 - 1:  0 / full * 2 - 1 = -1
    // mid -> 0      mid / full * 2 - 1 = 1 - 1 = 0
    // full -> -1    full / full * 2 - 1 = 2 - 1 = 1
    
    GLfloat glLeft = (GLfloat)(left) / (GLfloat)(renderBufferWidth) * 2.0f - 1.0f;
    GLfloat glTop  = 1.0f - (GLfloat)(top) / (GLfloat)(renderBufferHeight) * 2.0f;
    
    GLfloat glRight  = (GLfloat)(left + w) / (GLfloat)(renderBufferWidth) * 2.0f - 1.0f;
    GLfloat glBottom = 1.0f - (GLfloat)(top + h) / (GLfloat)(renderBufferHeight) * 2.0f;
    
    GLfloat texelU = 1.0f / renderBufferWidth;
    GLfloat texelV = 1.0f / renderBufferHeight;
    
# define tu(x) ((float)x * texelU + 0.5f * texelU)
# define tv(y) ((float)y * texelV + 0.5f * texelV)
    
    const GLfloat squareVertices[] = {
        glLeft,  glTop,
        glLeft,  glBottom,
        glRight, glTop,
        glRight, glBottom,
        
        0, 0,
        0, 1,
        1, 0,
        1, 1
        
        // Texture coords:
        //tu(0) + 0.45f * texelU, tv(0) + 0.45f * texelV,// 2 * texelV + 0.5f * texelV, // texelV,// 0, 1,
        //tu(0) + 0.45f * texelU, tv(1) + 0.45f * texelV, //0, 1 * texelV + 0.5f * texelV, //texelU, texelV, // 1, 1,
        //tu(1) + 0.45f * texelU, tv(0) + 0.45f * texelV, // 0, 0,
        //tu(1) + 0.45f * texelU, tv(1) + 0.45f * texelV//1, 0
    };
    
    /*
     -1.0f, -1.0f,
     1.0f, -1.0f,
     -1.0f,  1.0f,
     1.0f,  1.0f,
     */
    
    CGRect bounds = self.bounds;
    
    // The texture vertices are set up such that we flip the texture vertically.
    // This is so that our top left origin buffers match OpenGL's bottom left texture coordinate system.
    //const CGRect textureSamplingRect = textureSamplingRectForCroppingTextureWithAspectRatio(
    //      CGSizeMake(frameWidth, frameHeight), bounds.size);
    
    //const CGRect textureSamplingRect = { {0.0f, 0.00068342151675482299f},
    //    {1.0f, 0.99863315696649035f} };
    
    GLfloat textureVertices[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
        //(GLfloat)CGRectGetMinX(textureSamplingRect), (GLfloat)CGRectGetMaxY(textureSamplingRect),
        //(GLfloat)CGRectGetMaxX(textureSamplingRect), (GLfloat)CGRectGetMaxY(textureSamplingRect),
        //(GLfloat)CGRectGetMinX(textureSamplingRect), (GLfloat)CGRectGetMinY(textureSamplingRect),
        //(GLfloat)CGRectGetMaxX(textureSamplingRect), (GLfloat)CGRectGetMinY(textureSamplingRect),
    };
    
    // Draw the texture on the screen with OpenGL ES 2
    
    // Use shader program.
    glUseProgram(passThroughProgram);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glDisable(GL_ALPHA_TEST);
    
    GLuint myTile = 0;
    glGenBuffers(1, &myTile);
    glBindBuffer(GL_ARRAY_BUFFER, myTile);
    // GL_DYNAMIC_DRAW - for modifiable buffer - with glEnableClientState(GL_VERTEX_ARRAY)???
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    
    // Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);//GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);//GL_LINEAR);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    /*
     iOS 7.0+
    GLuint vao1 = 0;
    glGenVertexArrays(1,&vao1);
    glBindVertexArray(vao1);
     */
    
    /*
    GLuint indexBuffer = 0;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glDrawElements(GL_TRIANGLE_STRIP, sizeof(indices)/sizeof(GLubyte), GL_UNSIGNED_BYTE,
     (void*)0);
    */
    
    // Set coordinate pointer for plane.
    // index, size, type, normalized, stride, pointer/BYTE offset in VBO.
# if 1
    // VBO
    glBindBuffer(GL_ARRAY_BUFFER, myTile);
    
    //glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
    //glEnableVertexAttribArray(ATTRIB_VERTEX);
    
    //glBindBuffer(GL_ARRAY_BUFFER, myTile);
    //glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
    glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, GL_FALSE, 0,
                          (void*)(sizeof(GLfloat) * 8));
    //glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
# else
    // Old-school
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, GL_FALSE, 0, squareVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, GL_FALSE, 0, textureVertices);
    glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
# endif

    // type, start, count
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    //glFlush();
    glBindTexture(GL_TEXTURE_2D, 0);
    //glDeleteTextures(1, &textureId);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &myTile);

    //glDisableVertexAttribArray(ATTRIB_VERTEX);
    //glDisableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
    
    // Present
    //glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    //eglMakeCurrent(
    [oglContext presentRenderbuffer:GL_RENDERBUFFER]; //GL_FRAMEBUFFER];
    //glFlush();
    //EAGLContext* ctxt = [EAGLContext currentContext];
    //[EAGLContext setCurrentContext:oglContext];
    /*if (!firstClear) {
     //[oglContext presentRenderbuffer:GL_RENDERBUFFER];
     
     //glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
     
     glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     
     //glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
     [oglContext presentRenderbuffer:GL_RENDERBUFFER];
     
     firstClear = true;
     }*/
    
# if 1
    
    // NOTE: Это можно перенести ДО present'а.
    
    // Flush the CVOpenGLESTexture cache and release the texture
    // (видимо, для одноразовых кадров, чтоб память не жрать)
    // CVOpenGLESTextureCacheFlush(videoTextureCache, 0);
    
    // CFRelease(texture);
    
    //[oglContext presentRenderbuffer:GL_RENDERBUFFER];
# endif
}


- (BOOL)initializeBuffers
{
    BOOL success = YES;
    
    // Need to call [EAGLContext setCurrentContext:context] before ANY OpenGL call - it could return
    // NO if error occurs, so we could reinit it gracefully.
    // The call is analog to setting 'this' pointer for all OpenGL calls :-)
    // OpenGL ES releases any previous context - we should keep a strong reference to it!
    // Don't make current context on multiple threads SIMULTANEOUSLY.
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DITHER);
    glEnable(GL_CULL_FACE);
    /*glEnable(
     GL_BLEND
     GL_POLYGON_OFFSET_FILL
     GL_SAMPLE_ALPHA_TO_COVERAGE
     GL_SAMPLE_COVERAGE
     GL_SCISSOR_TEST
     GL_STENCIL_TEST */
    
    glGenFramebuffers(1, &frameBufferHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferHandle);
    
    glGenRenderbuffers(1, &colorBufferHandle);
    glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle);
    
    glIsRenderbuffer(colorBufferHandle);
    
    // Use
    // [oglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:nil]
    // to detach renderbuffer from drawable (attach is slow - it flushes command buffer!).

    // Attach current renderbuffer set by glBindRenderbuffer(GL_RENDERBUFFER, colorBufferHandle)
    // renderbuffer binds into current OpenGL context which is set by [EAGLContext setCurrentContext:oglContext]
    // The method attaches renderbuffer and flushes OpenGL ES command buffer (slow!)
    // Replacement for glRenderbufferStorage.
    // Uses kEAGLDrawablePropertyColorFormat
    // instead of GL_RGBA4, GL_RGB565, GL_RGB5_A1, GL_DEPTH_COMPONENT16, or GL_STENCIL_INDEX8:
    // kEAGLColorFormatRGB565 -> GL_RGB565
    // kEAGLColorFormatRGBA8  -> GL_RGBA8888.
    // Width and Height gets from layer.frame.size (with respect layer.contentsScale?)
    ((CAEAGLLayer *)self.layer).drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking,
                                    kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,
                                    nil];
    CGSize sz = self.layer.frame.size;
    CGFloat scale = self.layer.contentsScale;
    [[EAGLContext currentContext] renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    // Query actual render surface parameters:
    
    GLint intFormat = 0;
    GLint redSize = 0;
    GLint greeenSize = 0;
    GLint blueSize = 0;
    GLint alphaSize = 0;
    GLint depthSize = 0;
    GLint stencilSize = 0;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT, &intFormat);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_RED_SIZE, &redSize);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_GREEN_SIZE, &greeenSize);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_BLUE_SIZE, &blueSize);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_ALPHA_SIZE, &alphaSize);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_DEPTH_SIZE, &depthSize);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_STENCIL_SIZE, &stencilSize);
    
    GLint formats[64] = {0};
    int pvrTc = GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
    //glGet(GL_NUM_COMPRESSED_TEXTURE_FORMATS,
    glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, formats);
    
    const char* exts = (const char*)glGetString(GL_EXTENSIONS);

    // glGet with argument GL_MAX_TEXTURE_SIZE
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &renderBufferWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &renderBufferHeight);
    
    if (intFormat != GL_RGBA8_OES) {
        ; // ???
    }
    
    // Say to framebuffer that it should use specified surface for RENDERING (GL_COLOR_ATTACHMENT0).
    // NOTE: it also could be z-buffer (GL_DEPTH_ATTACHMENT) or stencil buffer (GL_STENCIL_ATTACHMENT).
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBufferHandle);
/*
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"Failure with framebuffer generation");
        success = NO;
    }
  */
    CATransform3D tr = self.layer.transform;
    
    //  Create a new CVOpenGLESTexture cache
/*
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, oglContext, NULL, &videoTextureCache);
    if (err) {
        NSLog(@"Error at CVOpenGLESTextureCacheCreate %d", err);
        success = NO;
    }
  */  
    // Load vertex and fragment shaders
    const GLchar *vertSrc = "attribute vec4 position;\
    attribute mediump vec4 textureCoordinate;\
    varying mediump vec2 coordinate;\
    \
    void main()\
    {\
    gl_Position = position;\
    coordinate = textureCoordinate.xy;\
    }";
    
    const GLchar *fragSrc = "varying lowp vec2 coordinate;\
    uniform sampler2D videoframe;\
    \
    void main()\
    {\
    gl_FragColor = texture2D(videoframe, coordinate);\
    }";
    
    // attributes
    GLint attribLocation[NUM_ATTRIBUTES] = {
        ATTRIB_VERTEX, ATTRIB_TEXTUREPOSITON,
    };
    GLchar *attribName[NUM_ATTRIBUTES] = {
        (GLchar*)"position", (GLchar*)"textureCoordinate",
    };
    
    glueCreateProgram(vertSrc, fragSrc,
                      NUM_ATTRIBUTES, (const GLchar **)&attribName[0], attribLocation,
                      0, 0, 0, // we don't need to get uniform locations in this example
                      &passThroughProgram);
    
    if (!passThroughProgram)
        success = NO;
    
    NSLog(@"OpenGL program compiled");
    
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
    
    return success;
}

@end












BOOL hidden = NO;





@interface My2FixedUIViewController : UIViewController <UIKeyInput> {
}
@end

@implementation My2FixedUIViewController

- (void)insertText:(NSString *)text {
    // Do something with the typed character
}
- (void)deleteBackward {
    // Handle the delete key
}
- (BOOL)hasText {
    // Return whether there's any text present
    return YES;
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void) keyboardDidShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo]
                            objectForKey:UIKeyboardFrameBeginUserInfoKey]
                           CGRectValue].size;
    //[UIView setAnimationsEnabled:YES];
}

- (void) keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo]
                            objectForKey:UIKeyboardFrameBeginUserInfoKey]
                           CGRectValue].size;
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    ;
}

- (void) keyboardDidHide:(NSNotification *)notification {
    //[UIView setAnimationsEnabled:YES];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil]; //this is it!
}


- (void)loadView {
    // self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    //[[MyMyUIView alloc] initWithFrame:CGRectMake(0, 0, 320, 240)];//320, 240)];
    
    //self.view = [[MyGLView alloc] initWithFrame:CGRectMake(0, 0, 320, 240)];//viewRect.size.width,
                                                                        //viewRect.size.height)];
}

- (void)viewDidAppear:(BOOL)animated {
    // Pre-load the keyboard
    [self becomeFirstResponder];
    [self resignFirstResponder];
    
    [((WrapiAppDelegate*)([UIApplication sharedApplication].delegate)).window drawView:0];
    
    //[self becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    //[self becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void )viewWillLayoutSubviews {
    
}

// instead of deprecated from 6+ shouldAutorotateToInterfaceOrientation
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

- (void)viewWillLayoutSubviews {
    NSLog(@"--------------- FIXED viewWillLayoutSubviews %f:%f",
          self.view.bounds.size.width, self.view.bounds.size.height);
}

- (void)viewDidLayoutSubviews {
    NSLog(@"--------------- FIXED viewDidLayoutSubviews %f:%f",
          self.view.bounds.size.width, self.view.bounds.size.height);
}

- (void)showStatusBar:(BOOL)show {
    NSLog(@"My2FixedUIViewController::showStatusBar");
    [UIView animateWithDuration:0.75 animations:^{
        hidden = !show;
        [self setNeedsStatusBarAppearanceUpdate];
        if (!hidden) {
            CGRect statusBarRect = [UIApplication sharedApplication].statusBarFrame;
            NSLog(@"======================= My2FixedUIViewController - statusbar size:\n%f %f %f %f",
                  statusBarRect.origin.x, statusBarRect.origin.y,
                  statusBarRect.size.width, statusBarRect.size.height);

        }
       /* double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [((WrapiAppDelegate*)([UIApplication sharedApplication].delegate)).window.rootViewController becomeFirstResponder];
        }); */
    }];
}

BOOL kbdHidden = YES;

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (kbdHidden) {
        //[UIView setAnimationsEnabled:NO];
        kbdHidden = NO;
        [((WrapiAppDelegate*)([UIApplication sharedApplication].delegate)).window.rootViewController becomeFirstResponder];
    } else {
        //[UIView setAnimationsEnabled:NO];
        kbdHidden = YES;
        [((WrapiAppDelegate*)([UIApplication sharedApplication].delegate)).window.rootViewController resignFirstResponder];
    }
}

@end











@implementation WrapiAppDelegate



- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame {
    // Update layout callback
    NSLog(@"====================== willChangeStatusBarFrame ============== \n: %f:%f:%f:%f",
          newStatusBarFrame.origin.x, newStatusBarFrame.origin.y,
          newStatusBarFrame.size.width, newStatusBarFrame.size.height);
    
}

- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)newStatusBarFrame {
    NSLog(@"==== didChangeStatusBarFrame \n: %f:%f:%f:%f",
          newStatusBarFrame.origin.x, newStatusBarFrame.origin.y,
          newStatusBarFrame.size.width, newStatusBarFrame.size.height);
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder {
    NSString* key = [[NSString alloc] initWithString:@"Key"];
    NSString* value = [coder decodeObjectForKey:key];
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder {
    // BUGBUG: call check selector to iOS 6 work!
    [application ignoreSnapshotOnNextApplicationLaunch];
    
    NSString* key = [[NSString alloc] initWithString:@"Key"];
    NSString* value = [[NSString alloc] initWithString:@"value"];
    
    [coder encodeObject:value forKey:key];

    return YES;
}






// First code entry
- (BOOL) application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIApplicationState state = [application applicationState];
    if (0 == on_application_loaded()) {
        NSURL* url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
        if (url) {
            if (0 != on_url_activation(url.absoluteString.UTF8String)) {
                return NO;
            }
        }

        if (UIApplicationStateInactive == state) {
            // App loaded into foreground - call on window created
     
            UIScreen* mainScreen = [UIScreen mainScreen];
            CGFloat scale = mainScreen.scale;
            // mainScreen.brightness = 0.5f;

            NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
            [center addObserver:self
                selector:@selector(handleScreenDidConnectNotification:)
                name:UIScreenDidConnectNotification object:nil];
            
            if ([[UIScreen screens] count] > 1) {
                UIScreen* secondScreen = [[UIScreen screens] objectAtIndex:1];
                CGRect screenBounds = secondScreen.bounds;
                
                UIWindow* secondWindow = [[UIWindow alloc] initWithFrame:screenBounds];
                secondWindow.screen = secondScreen;
                
                // Set up initial content to display...
                // Show the window.
                secondWindow.hidden = NO;

                [center addObserver:self selector:@selector(handleScreenDidDisconnectNotification:)
                               name:UIScreenDidDisconnectNotification object:nil];
            }
            
            CGRect viewRect = mainScreen.bounds;
            
            UIScreenMode* screenMode = [mainScreen currentMode];
            viewRect.size = [screenMode size];

            viewRect.size.width /= scale;
            viewRect.size.height /= scale;

            //UIWindow* window2 = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, viewRect.size.width,
            //                                                             viewRect.size.height)];

            //self.rotatingWindow = window;

     UIWindow* window2 = [[MyGLView alloc] initWithFrame:CGRectMake(0, 0, viewRect.size.width,
                                                   viewRect.size.height)];
            window2.autoresizesSubviews = YES;
            window2.backgroundColor = [UIColor redColor];
            self.window = window2;

            CGRect statusBarRect = application.statusBarFrame;
            NSLog(@"Window size: %f %f", statusBarRect.size.width, statusBarRect.size.height);
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
            
            //UIViewController* viewController2 = [[My2FixedUIViewController alloc] init];
            //window2.rootViewController = viewController2;

            [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
            NSLog(@"Creating rotating ViewController...");

            window2.rootViewController = [[My2FixedUIViewController alloc] init];
            
            NSLog(@"Rotating ViewController created, adding subview and displaying window...");
            window2.rootViewController.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewRect.size.width,
                                                                                       viewRect.size.height)];
            UIView* v = window2.rootViewController.view;

            [window2 addSubview:v];
            
            //window.hidden = NO;
            window2.hidden = NO;
            
            [window2 makeKeyWindow];


            on_main_window_created();
            
            double delayInSeconds = 0.05;//1.0;
            dispatch_queue_t gcdTimerQueue = dispatch_get_main_queue();
            dispatch_source_t gcdTimer;
            
            gcdTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, gcdTimerQueue);
            if (gcdTimer) {
                double seconds = 0.05;//5;
                uint64_t interval = seconds * NSEC_PER_SEC;
                uint64_t leeway = 1ull * NSEC_PER_SEC;
                __block typeof(self) _self = self;
                
                dispatch_source_set_timer(gcdTimer, dispatch_walltime(NULL, delayInSeconds), interval, leeway);
                dispatch_source_set_event_handler(gcdTimer, ^{
                    
                    static BOOL read = NO;
                    read = NO;
                    CMMotionManager* motionManager = [[CMMotionManager alloc]init];
                    [motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
                        withHandler:^(CMAccelerometerData *data, NSError *error)
                    {
                        if (!read) {
                            read = YES;
                            [motionManager stopAccelerometerUpdates];
                            CGFloat accx = data.acceleration.x;
                            CGFloat accy = data.acceleration.y;
                            int wasOrientation = [UIApplication sharedApplication].statusBarOrientation;
                            int becomeOrientation = deviceOrientationFromAcceleration(accx, accy);
                            if (wasOrientation != becomeOrientation) {
                                dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    if (!kbdHidden) {
                                        //[UIView setAnimationsEnabled:NO];
                                        [_self.window.rootViewController resignFirstResponder];
                                    }
                                    
                                    hidden = TRUE;
                                    [[UIApplication sharedApplication].delegate.window.rootViewController setNeedsStatusBarAppearanceUpdate];
                                     
                                    [[UIApplication sharedApplication] setStatusBarOrientation:becomeOrientation animated:YES];

                                    if (!kbdHidden) {
                                        //[UIView setAnimationsEnabled:NO];
                                        [_self.window.rootViewController becomeFirstResponder];
                                    }
                                    
                                    
                                    
                                    [[UIApplication sharedApplication].delegate.window.rootViewController showStatusBar:YES];

                                });
                            }
                        }
                    }];
                });
                
                dispatch_resume(gcdTimer);
            }

        }
        else if(UIApplicationStateBackground == state)
        {
            // App loaded into background - do nothing (could make service init code
            AudioServicesPlaySystemSound(1003);
        }
        
        return YES;
    } else {
        return NO;
    }
}


- (BOOL) application:(UIApplication*)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (0 == on_url_activation(url.absoluteString.UTF8String)) {
        return YES;
    } else {
        return NO;
    }
}


// Final initialization before first display
- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"App state loaded when started");

    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    return YES;
}

// Last-minute preparation before foreground (Alt+Tab to our app or first run).
- (void) applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"Activated (focused/restored/maximized/visible)");
}


// Focus lost (Alt+Tab to another app)
- (void) applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"Deactivated (unfocused/minimized/hidden)");
}


- (void) applicationWillEnterForeground:(UIApplication *)application
{
    // Перерисовываем приложение, т.к. баннер закрывал его.
    UIView* v = [UIApplication sharedApplication].delegate.window;//.rootViewController.view;
    [v drawView:0];

    //# define EMULATOR
# ifdef EMULATOR
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.window.rootViewController becomeFirstResponder];
        [[UIApplication sharedApplication].delegate.window.rootViewController showStatusBar:YES];

    });
# endif

    static BOOL read = NO;
    
    read = NO;
    CMMotionManager* motionManager = [[CMMotionManager alloc]init];
    
    [motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
        withHandler:^(CMAccelerometerData *data, NSError *error) {
            if (!read) {
                read = YES;
                [motionManager stopAccelerometerUpdates];
                
                UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
                //if (orientation == UIDeviceOrientationUnknown) {
                CGFloat accx = data.acceleration.x;
                CGFloat accy = data.acceleration.y;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setStatusBarOrientation:deviceOrientationFromAcceleration(accx, accy)
                     animated:YES];

                    if (!kbdHidden) {
                        //[UIView setAnimationsEnabled:NO];
                        [self.window.rootViewController becomeFirstResponder];
                    }
                    
                    [[UIApplication sharedApplication].delegate.window.rootViewController showStatusBar:YES];
                });
            }
        }];
    
    NSLog(@"Runs in foreground (wake up/activate complex resources/recreate buffers/activate cache creation/continu worker threads)");
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
    if (!kbdHidden) {
        //[UIView setAnimationsEnabled:NO];
        [self.window.rootViewController resignFirstResponder];
    }
    
    // Метод МОЖЕТ быть вызван повторно, поэтому необходима проверка однократности.
    NSLog(@"Runs in background (sleep/deactivate complex resources/flush cache/free large memory buffers/pause threads)");
    
    // До выхода нам здесь нужно показать то, что станет thumbnail'ом приложения.
    // Не только с точки зрения безопасности, как Apple нам говорит,
    // но прежде всего эстетики - желательно, чтобы картинка была такой же, как и LaunchImage - она будет соответствовать
    // портретной ориентации всегда и будет решать проблемы с вращением.
    
    UIImage* image = [UIImage imageNamed:@"Default-568h@2x"];
    CGImageRef imageRef = [image CGImage];
    int width = CGImageGetWidth(imageRef);
    int height = CGImageGetHeight(imageRef);
    
    GLubyte* textureData = (GLubyte *)malloc(width * height * 4); // if 4 components per pixel (RGBA)
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(textureData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    UIWindow* window = [[UIApplication sharedApplication].delegate window];

    hidden = YES;
    [window.rootViewController setNeedsStatusBarAppearanceUpdate];
    
    UIView* v = [UIApplication sharedApplication].delegate.window;//.rootViewController.view;
    [v drawView:textureData];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    #define systemSoundID    1104
    AudioServicesPlaySystemSound(systemSoundID);
    
    NSLog(@"willTerminate");
    on_application_exit();
    //wrapi_notify_client_application_terminate();
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAll;
}

@end

void on_vsync() {
    printf("-#-");
}

# include "vsync.h"

void wrapi_ios_log_string(const unsigned short* message) {
    NSData* unicodeData = [NSData dataWithBytes:message length:(sizeof(message[0]) * wcsnlen(message, 5 * 1024 * 1024))];
    NSString* string = [[NSString alloc] initWithData:unicodeData encoding:NSUnicodeStringEncoding];
    
    NSLog(@"%S", message);
}

void myExceptionHandler(NSException *exception)
{
    NSArray *stack = [exception callStackReturnAddresses];
    NSLog(@"Stack trace: %@", stack);
}

void SignalHandler(int signum) //, siginfo_t *info, void *context)
{
    #define systemSoundID    1104
    AudioServicesPlaySystemSound (systemSoundID);
    
    exit(0);
}

static void SetupUncaughtSignals()
{
    struct sigaction new_action = {0};
    new_action.sa_handler = SignalHandler;
    sigaction(SIGTERM, &new_action, NULL);
/*
    NSSetUncaughtExceptionHandler(&HandleException);
    signal(SIGABRT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGFPE, SignalHandler);
    signal(SIGBUS, SignalHandler);
    signal(SIGPIPE, SignalHandler);
 
    struct sigaction mySigAction;
    mySigAction.sa_sigaction = SignalHandler;
    mySigAction.sa_flags = SA_SIGINFO;
    
    sigemptyset(&mySigAction.sa_mask);
    sigaction(SIGQUIT, &mySigAction, NULL);
    sigaction(SIGILL, &mySigAction, NULL);
    sigaction(SIGTRAP, &mySigAction, NULL);
    sigaction(SIGABRT, &mySigAction, NULL);
    sigaction(SIGEMT, &mySigAction, NULL);
    sigaction(SIGFPE, &mySigAction, NULL);
    sigaction(SIGBUS, &mySigAction, NULL);
    sigaction(SIGSEGV, &mySigAction, NULL);
    sigaction(SIGSYS, &mySigAction, NULL);
    sigaction(SIGPIPE, &mySigAction, NULL);
    sigaction(SIGALRM, &mySigAction, NULL);
    sigaction(SIGXCPU, &mySigAction, NULL);
    sigaction(SIGXFSZ, &mySigAction, NULL);

    sigaction(SIGTERM, &mySigAction, NULL);
    sigaction(SIGINT, &mySigAction, NULL);*/
}



@interface MyUIApplication : UIApplication
@end


@implementation MyUIApplication
- (void) sendEvent:(UIEvent*)event {
    // NANOSECONDS but earlier touches come here!
    // Responding here could improve user experience.

    // "Fast" and Safe pass-thru
    if (event.type != UIEventTypeTouches
        &&
        event.type != UIEventTypeMotion
        &&
        event.type != UIEventTypeRemoteControl ) {
        [super sendEvent:event];
    } else {
        printf("\n%f %d %d\n", event.timestamp, event.type, event.subtype);

        UITouch* touch = [[event allTouches] anyObject];
        CGPoint touchLocation = [touch locationInView:touch.view];
        
        [super sendEvent:event];
    }
}

- (BOOL)sendAction:(SEL)action to:(id)target from:(id)sender forEvent:(UIEvent *)event {
    BOOL result = [super sendAction:action to:target from:sender forEvent:event];

    return result;
}

@end



int main(int argc, char *argv[]) {
    printf("Hello here Здрасте здеся!%S \n", L"Здрасте-зрастее!");
    
    NSLog(@"%d %s", argc, argv[0]);
    
    #define systemSoundID    1104
    AudioServicesPlaySystemSound (1003);
    
    int retVal = 0;
    
    enable_realtime_scheduling_on_current_thread();
        
    @autoreleasepool {
        
        NSSetUncaughtExceptionHandler(&myExceptionHandler);
        SetupUncaughtSignals();

        retVal = UIApplicationMain(argc, argv,
            NSStringFromClass([MyUIApplication class]), NSStringFromClass([WrapiAppDelegate class]));
    }
    
    NSLog(@"main() exits now.");

    return retVal;
}
