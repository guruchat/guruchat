package org.guruchat.wrapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

public final class WrapiAutorun extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		/****** For Start Activity *****/
        //Intent i = new Intent(context, WrapiApplication.class);  
        //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //context.startActivity(i);  

       /***** For start Service  ****/
       Intent myIntent = new Intent(context, WrapiBackgroundService.class);
       context.startService(myIntent);
	}

}
