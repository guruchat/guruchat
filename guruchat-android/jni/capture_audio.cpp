#include <stdlib.h>
#include <jni.h>
#include <string.h>
#include <stdio.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

#include <SLES/OpenSLES_AndroidConfiguration.h>
//#include <SLES/OpenSLES_AndroidMetadata.h>

# include <sys/select.h>
# include <sys/socket.h>
# include <arpa/inet.h>
# include <netinet/in.h>

//#include <stddef.h>

short buffer[ 644 ] = {0};
short buffer2[ 644 ] = {0};

sockaddr_in servaddr = {0};
int sock_fd = -1;

void CreateSocket()
{
	sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	sockaddr_in myaddr = {0};

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(8888);
	servaddr.sin_addr.s_addr = inet_addr ("192.168.137.1");
}

unsigned long long packetNumber = 0;

void RecorderSimpleBufferQueueCallback(SLAndroidSimpleBufferQueueItf queueItf, void *pContext)
{
	static int curBuf = 1;
	short* buf = curBuf ? buffer : buffer2;
	if (curBuf)
	{
		(**queueItf).Enqueue(queueItf, (void*) buffer2, 1280);
		curBuf = 0;
	}
	else
	{
		(**queueItf).Enqueue(queueItf, (void*) buffer, 1280);
		curBuf = 1;
	}

	*((unsigned long long*)&buf[640]) = packetNumber;
	ssize_t retval = sendto(sock_fd, (const void*)buf, (size_t)(1280 + 8), 0, (const struct sockaddr*)&servaddr, (socklen_t)sizeof(servaddr));

	++packetNumber;

	//(**queueItf).Clear(queueItf);
}


void StartRecording()
{
	//const int MAX_NUMBER_INPUT_DEVICES = 16;

	// create engine
	SLObjectItf audioEngineObject = 0;
	SLresult result = slCreateEngine(&audioEngineObject, 0, 0, 0, 0, 0);
	if (SL_RESULT_SUCCESS == result && audioEngineObject && *audioEngineObject)
	{
		// realize the engine
		result = (**audioEngineObject).Realize(audioEngineObject, SL_BOOLEAN_FALSE);
		if (SL_RESULT_SUCCESS == result)
		{
			SLEngineItf engine = 0;
			result = (**audioEngineObject).GetInterface(audioEngineObject, SL_IID_ENGINE, &engine);
			if (SL_RESULT_SUCCESS == result && engine && *engine)
			{
				SLDataLocator_IODevice microphoneDevice = {0};
				microphoneDevice.locatorType = SL_DATALOCATOR_IODEVICE;
				microphoneDevice.deviceType = SL_IODEVICE_AUDIOINPUT;
				microphoneDevice.deviceID = SL_DEFAULTDEVICEID_AUDIOINPUT;
				microphoneDevice.device = 0;
				SLDataSource captureSrcDesc = {&microphoneDevice, 0};

				SLDataLocator_AndroidSimpleBufferQueue simpleBufferQueueDesc = {0};
				simpleBufferQueueDesc.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
				simpleBufferQueueDesc.numBuffers = 2;

				// Format is enumerable (see WebRTC for samples)!
				SLDataFormat_PCM pcmFormat = {0};
				pcmFormat.formatType = SL_DATAFORMAT_PCM;
				pcmFormat.numChannels = 1;
				pcmFormat.samplesPerSec = SL_SAMPLINGRATE_16;
				pcmFormat.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
				pcmFormat.containerSize = 16;
				pcmFormat.channelMask = SL_SPEAKER_FRONT_CENTER;
				pcmFormat.endianness = SL_BYTEORDER_LITTLEENDIAN;
				SLDataSink captureDstDesc = {&simpleBufferQueueDesc, &pcmFormat};

				const SLInterfaceID interfaceIds[1] = { SL_IID_ANDROIDSIMPLEBUFFERQUEUE };
				const SLboolean requiredIdsMap[1] = { SL_BOOLEAN_TRUE };
				SLObjectItf capturerObject = 0;
				result = (**engine).CreateAudioRecorder(engine, &capturerObject, &captureSrcDesc, &captureDstDesc, 1, interfaceIds, requiredIdsMap);
				if (SL_RESULT_SUCCESS == result && capturerObject && *capturerObject)
				{
					// Realizing the recorder in synchronous mode.
					result = (**capturerObject).Realize(capturerObject, SL_BOOLEAN_FALSE);
					if (SL_RESULT_SUCCESS == result)
					{
						// Get the RECORD interface - it is an implicit interface
						SLRecordItf controlRecording = 0;
						result = (**capturerObject).GetInterface(capturerObject, SL_IID_RECORD, (void*) &controlRecording);
						if (SL_RESULT_SUCCESS == result && controlRecording && *controlRecording)
						{
							SLAndroidSimpleBufferQueueItf capturedSamples = 0;
							result = (**capturerObject).GetInterface(capturerObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, (void*) &capturedSamples);
							if (SL_RESULT_SUCCESS == result && capturedSamples && *capturedSamples)
							{
								// Setup to receive buffer queue event callbacks
								result = (**capturedSamples).RegisterCallback(capturedSamples, RecorderSimpleBufferQueueCallback, 0);
								if (SL_RESULT_SUCCESS == result)
								{
									result = (**capturedSamples).Enqueue(capturedSamples, (void*) buffer, 1280);
									result = (**capturedSamples).Enqueue(capturedSamples, (void*) buffer2, 1280);

									CreateSocket();

									// Record the audio
									result = (**controlRecording).SetRecordState(controlRecording, SL_RECORDSTATE_RECORDING);
								}
							}
						}
					}
				}
			}
		}
	}
}
