LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

CXXFLAGS = -D__STDC_CONSTANT_MACROS -std=gnu++11
LOCAL_CFLAGS := -std=gnu++11
LOCAL_LDLIBS += -landroid
LOCAL_MODULE    := guruchat
LOCAL_SRC_FILES := guruchat.cpp video_capture_android.cpp capture_audio.cpp ./../../app.cpp
LOCAL_LDLIBS    += -lOpenSLES -lEGL -lGLESv1_CM -landroid # -lm -llog -ljnigraphics -lz

include $(BUILD_SHARED_LIBRARY)
