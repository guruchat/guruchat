package com.android.particles;

import java.util.List;

import com.android.particles.ParticlesView.MyInputConnection;

import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Looper;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;

public class MyApp extends Application {
  static 
  {
    System.loadLibrary("particles");
  }

  public MyApp() {
	  super();
	  //android.view.WindowManager
  }
  
 

  @Override
  public void onCreate() {
	String thisPackageFullPath = getPackageCodePath();
    super.onCreate();
    
    // First run after install (saving parameters to a file which uses from C++ only).
    
    // bi-planar:
    // Native Android
    int formatY2x2_VU1x1 = ImageFormat.NV21; // All devices, 2 planes 8 YYYYYYYY VUVUVUVU
    // Native Apple's
    // Apple's NV12 = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
    // 2 planes 8 YYYYYYYY UVUVUVUV
    int formatY2x2_UV1x1 = 0;
    
    // planar:
    // Android second standard (after API 12+)
    int formatY2x2_V1x1_U1x1 = ImageFormat.YV12; // 8+, 12+ std (3 planes, analog I420 with reversed U and V: 8 YYYYYYYY VVVV UUUU)
    
    // standard decoder's/encoder's format:
    int formatY2x2_U1x1_V1x1 = ImageFormat.YUV_420_888; // 19+ only, I420/IYUV = 3 planes 8 YYYYYYYY UUUU VVVV
    
    int formatY2x2_U1x2_V1x2 = 0; // YV16
    
    // NV16: Y_VU
    int formatY2x2_VU1x2 = ImageFormat.NV16; //WTF very strange

    // Compressed (webcams on windows often):
    int formatYUYV1x2 = ImageFormat.YUY2; // YUY2/YUYV 1 mixed plane, 8 YUYVYUYVYUYVYUYV
    
    // YVYU = 1 mixed plane, 8 YVYUYVYUYVYUYVYU
    int formatYVYU1x2 = 0;

    int nCameras = Camera.getNumberOfCameras();
    for (int nCamera = 0; nCamera < nCameras; ++nCamera) {
    	Camera camera = Camera.open(nCamera);
    	Camera.Parameters cameraParams = camera.getParameters();
    	camera.release();
    	int upExposureSteps = cameraParams.getMaxExposureCompensation();
    	int downExposureSteps = cameraParams.getMinExposureCompensation();
    	float exposureUnit = cameraParams.getExposureCompensationStep();
    	float exposure = exposureUnit * cameraParams.getExposureCompensation();
    	float maxExposure = upExposureSteps * exposureUnit;
    	float minExposure = downExposureSteps * exposureUnit;
    	int fpsRange[] = new int[2];
    	cameraParams.getPreviewFpsRange(fpsRange);
    	
    	List<int[]> supportedFpsRange = cameraParams.getSupportedPreviewFpsRange();
    	List<Integer> oldSupportedFpsRange = cameraParams.getSupportedPreviewFrameRates();
    	// old fps setting
    	int oldFps = cameraParams.getPreviewFrameRate();
    	float angle = cameraParams.getVerticalViewAngle();
    	List<Integer> cameraFormats = cameraParams.getSupportedPreviewFormats();
    	List<Camera.Size> sizes = cameraParams.getSupportedPreviewSizes();
    	String antibanding = cameraParams.getAntibanding();
    	float focalLength = cameraParams.getFocalLength();
    	String flashMode = cameraParams.getFlashMode();
    	float focusDistances[] = new float[3];
    	cameraParams.getFocusDistances (focusDistances);
    	String focusMode = cameraParams.getFocusMode();
    	float horizontalViewAngle = cameraParams.getHorizontalViewAngle();
    	int maxZoom = cameraParams.getMaxZoom();
    	Camera.Size sensorSize = cameraParams.getPictureSize();
    	int nPixels = 0;
    	for (Integer f : cameraFormats) {
    		nPixels = ImageFormat.getBitsPerPixel(f);
    	}
    }
    
    ActivityManager am = (ActivityManager)this.getSystemService(Context.ACTIVITY_SERVICE);
    ConfigurationInfo confInfo = am.getDeviceConfigurationInfo();
    assert(confInfo.reqGlEsVersion >= 0x20000);
    int memoryClass = am.getMemoryClass();
    ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
    am.getMemoryInfo(memInfo);
    long appMem = memInfo.availMem;
    //long deviceMem = memInfo.totalMem;
    
    PackageManager pm = this.getPackageManager();
    boolean safeMode = pm.isSafeMode();
    // exit, we don't support the feature yet or always.
    
    String packageName = getPackageName();
    try {
        PackageInfo p = getApplicationContext().getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        for (ActivityInfo activityInfo : p.activities) {
            Log.i("MyApp", "ACT " + activityInfo.name+" "+activityInfo.packageName);
        }
    } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
    }
    
    ComponentName myActivityName = new ComponentName("com.android.particles", "com.android.particles.ParticlesActivity");
    
    pm.setComponentEnabledSetting(myActivityName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
    		PackageManager.DONT_KILL_APP);
    
    FeatureInfo[] features = pm.getSystemAvailableFeatures();
    // Device's overall service Java (!!!) shared libs
    String[] sharedLibs = pm.getSystemSharedLibraryNames();
    
    // For checking app manifest and help fix it
    ApplicationInfo appInfo = this.getApplicationInfo();
    // For checking app manifest and help fix it
    try {
    	ActivityInfo actInfo = pm.getActivityInfo(myActivityName,
				PackageManager.GET_META_DATA
				| PackageManager.GET_INTENT_FILTERS
				| PackageManager.GET_SHARED_LIBRARY_FILES);
    	int configChanges = actInfo.configChanges;
    	
    	/* Add
        mcc|mnc|locale|touchscreen|keyboard|keyboardHidden|navigation|orientation|screenLayout|uiMode|fontScale
    	to the AndroidManifest.xml in application.activity.android:configChange attribute */
    	
    	// fontScale - The font scaling factor has changed, that is the user has selected a new global font size.
    	assert((ActivityInfo.CONFIG_FONT_SCALE & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_MCC & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_MNC & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_LOCALE & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_TOUCHSCREEN & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_KEYBOARD & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_KEYBOARD_HIDDEN & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_NAVIGATION & configChanges) != 0);
    	assert((ActivityInfo.CONFIG_ORIENTATION & configChanges) != 0);
    	// The screen layout has changed. This might be caused by a different display being activated.
    	assert((ActivityInfo.CONFIG_SCREEN_LAYOUT & configChanges) != 0);
    	// The global user interface mode has changed.
    	// For example, going in or out of car mode, night mode changing, etc.
    	assert((ActivityInfo.CONFIG_UI_MODE & configChanges) != 0);
    	
    	
    	// 13: |screenSize|smallestScreenSize
    	// assert((ActivityInfo.CONFIG_SCREEN_SIZE & configChanges) != 0);
    	// assert((ActivityInfo.CONFIG_SMALLEST_SCREEN_SIZE & configChanges) != 0);
    	
    	// 17: |layoutDirection
    	// assert((ActivityInfo.CONFIG_DENSITY & configChanges) != 0);
    	// The layout direction has changed. For example going from LTR to RTL.
    	// assert((ActivityInfo.CONFIG_LAYOUT_DIRECTION & configChanges) != 0);
    	
	} catch (NameNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    WindowManager wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
    
    // getMainLooper().quitSafely();
    // throw new IllegalStateException();

    // API level 10
    //int minSupportedVersion = android.os.Build.VERSION_CODES.GINGERBREAD_MR1;
    
    //assert((MyApp)(this.getApplicationContext()) == this);
  }
  
  @Override
  public void onLowMemory () {

  }
  
  @Override
  public void onTerminate () {
	  
  }
  
  @Override
  public void onConfigurationChanged(Configuration newConfig) {       
      super.onConfigurationChanged(newConfig);

      // -- stomp any attempts by the OS to resize my view.
      // -- Hardcoded values for illustration only. In real life
      // -- you'll need to save these when the surface view is originally created
      //mView.getLayoutParams().width = 800;
      //mView.getLayoutParams().height = 480;
  }
  /*
  @Override public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);

      if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      }
      else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
      }
  }
*/

}