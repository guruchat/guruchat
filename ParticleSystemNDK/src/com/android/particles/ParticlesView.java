/* Copyright (c) <2012>, Intel Corporation
 *
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Intel Corporation nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.android.particles;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.pm.ActivityInfo;
//import android.opengl.GLSurfaceView;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.view.inputmethod.InputMethodManager;

import javax.microedition.khronos.egl.EGLConfig;
//import javax.microedition.khronos.egl.EGLDisplay;
//import android.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.os.SystemClock;





//import com.android.particles.ParticlesActivity;
import com.android.particles.*;
///////////////////////////////////////////////////////////////////////////////////////////////////
// ParticlesView - main view that extends GLSurfaceView
class ParticlesView extends GLSurfaceView implements KeyListener
{
	private InputMethodManager imm;
	
	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {

	    // We set manual input connection to catch all keyboard interaction
	    return new MyInputConnection(super.onCreateInputConnection(outAttrs), true);

	}
	
	@Override
	public boolean onCheckIsTextEditor() {
	    return true;
	}
	
	class MyInputConnection extends InputConnectionWrapper {

		public MyInputConnection(InputConnection target, boolean mutable) {
		    super(target, mutable);
		}
		
		@Override
		public boolean reportFullscreenMode(boolean enabled) {
			return true;
		}

		@Override
		public boolean setComposingText(CharSequence text, int newCursorPosition)
		{
		    //Log.d(TAG, "setComposingText " + text);
		    return super.setComposingText(text, newCursorPosition);
		}

		@Override
		public boolean commitText(CharSequence text, int newCursorPosition)
		{
		    //Log.d(TAG, "commitText " + text);
		    return super.commitText(text, newCursorPosition);
		}

		@Override
		public boolean sendKeyEvent(KeyEvent event) {
		    return super.sendKeyEvent(event);
		}


		@Override
		public boolean deleteSurroundingText(int beforeLength, int afterLength) {       
		    if (beforeLength == 1 && afterLength == 0) {
		            ;//Log.d(TAG, "DELETE!");
		    }

		    return super.deleteSurroundingText(beforeLength, afterLength);
		}
		
		@Override
		public CharSequence getTextBeforeCursor(int n, int flags) {
			return "ABC";
		}
		
		@Override public ExtractedText getExtractedText(ExtractedTextRequest r, int b) {
			return new ExtractedText();
		}
		
		@Override public boolean finishComposingText() {
			return false;
		}
		
	}
	
	private boolean created;

    public ParticlesView( Context context, InputMethodManager imm ) 
    {
        super( context );
        
        this.created = false;
        
        this.imm = imm;
        
        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion( 2 );

        // Set the renderer associated with this view
        setRenderer( new ParticlesRenderer() );
        setRenderMode(RENDERMODE_WHEN_DIRTY);
        this.requestRender();
    }
    
    public void onStartTemporaryDetach() {
    	super.onStartTemporaryDetach();
    }
    
    public void setRotation(float rotation) {
    	super.setRotation(rotation);
  
    }
    /*
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            Activity a = getActivity();
            if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
*/
    
    
    private static class ParticlesRenderer implements GLSurfaceView.Renderer 
    {
        private long m_nLastTime;

        public void onDrawFrame( GL10 gl )
        {
            // calculate elapsed time
            /*if( m_nLastTime == 0 )
                m_nLastTime = SystemClock.elapsedRealtime();

            long nCurrentTime = SystemClock.elapsedRealtime();
            long nElapsedTime = nCurrentTime - m_nLastTime;
            float fElapsedTime = nElapsedTime / 1000.0f;
            m_nLastTime = nCurrentTime;
*/
            ParticlesLib.step( 1.0f / 30.0f );
        }

        public void onSurfaceChanged( GL10 gl, int width, int height )
        {
        	//ParticlesLib.init( width, height, 0, 0);
        }

		@Override
		public void onSurfaceLost() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void loadTextures(GL10 gl, TextureLibrary library) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void flushTextures(GL10 gl, TextureLibrary library) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void loadBuffers(GL10 gl, BufferLibrary library) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void flushBuffers(GL10 gl, BufferLibrary library) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config,
				javax.microedition.khronos.egl.EGLDisplay display,
				javax.microedition.khronos.egl.EGLSurface surface) {
			
			ParticlesLib.init( 800, 800, display.hashCode(), surface.hashCode());
		}
    }
    
    public void requestLayout() {
    	super.requestLayout();
    }
    /*
    @Override
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (changed) {
            (this).layout(0, 0, 480, 480);
        }
    }
    */
    public void invalidate() {
    	super.invalidate();
    }
    
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    	super.surfaceCreated(holder);
    /*	
    	WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	display.getRotation();
    	Surface.setOrientation(display.getDisplayId(), display.getRotation());
    	*/
    	holder.setFixedSize(800, 800);
    	holder.setKeepScreenOn(true);
    }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    	if (!this.created) {
    		this.created = true;
    		super.surfaceChanged(holder, format, w, h);
    	}
    	//holder.setFixedSize(480, 800);
    }
    
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    	super.surfaceDestroyed(holder);
    }

	@Override
	public void clearMetaKeyState(View arg0, Editable arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getInputType() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean onKeyDown(View view, Editable text, int keyCode,
			KeyEvent event) {
		/*
		ActivityManager am = (ActivityManager)view.getContext().getSystemService(Context.ACTIVITY_SERVICE);
		ParticlesActivity cn = (ParticlesActivity)(am.getRunningTasks(1).get(0).topActivity);
		.getApplicationContext().getCurrentActivity() */
		
		return false;
	}

	@Override
	public boolean onKeyOther(View view, Editable text, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}

