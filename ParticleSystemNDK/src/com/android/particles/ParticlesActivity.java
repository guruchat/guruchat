/* Copyright (c) <2012>, Intel Corporation
 *
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Intel Corporation nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.android.particles;

import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

import android.app.Activity;
import android.app.Service;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Rect;
import android.inputmethodservice.InputMethodService;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;
import android.view.Choreographer;
import android.view.InputQueue.Callback;

class MyInputConnection extends InputConnectionWrapper {

	public MyInputConnection(InputConnection target, boolean mutable) {
	    super(target, mutable);
	}
	
	@Override
	public boolean reportFullscreenMode(boolean enabled) {
		return true;
	}

	@Override
	public boolean setComposingText(CharSequence text, int newCursorPosition)
	{
	    //Log.d(TAG, "setComposingText " + text);
	    return super.setComposingText(text, newCursorPosition);
	}

	@Override
	public boolean commitText(CharSequence text, int newCursorPosition)
	{
	    //Log.d(TAG, "commitText " + text);
	    return super.commitText(text, newCursorPosition);
	}

	@Override
	public boolean sendKeyEvent(KeyEvent event) {
	    return super.sendKeyEvent(event);
	}


	@Override
	public boolean deleteSurroundingText(int beforeLength, int afterLength) {       
	    if (beforeLength == 1 && afterLength == 0) {
	            ;//Log.d(TAG, "DELETE!");
	    }

	    return super.deleteSurroundingText(beforeLength, afterLength);
	}
	
	@Override
	public CharSequence getTextBeforeCursor(int n, int flags) {
		return "ABC";
	}
	
	@Override public ExtractedText getExtractedText(ExtractedTextRequest r, int b) {
		return new ExtractedText();
	}
	
	@Override public boolean finishComposingText() {
		return false;
	}
	
}

public class ParticlesActivity extends Activity implements SurfaceHolder.Callback2, KeyEvent.Callback,
  //android.view.View.OnCreateContextMenuListener,
  android.view.Window.Callback
//KeyListener//, InputQueue.Callback

{
    static AssetManager sAssetManager;
    ParticlesView mView;
    
    @Override
    public boolean dispatchTouchEvent (MotionEvent event) {
    	if (!super.dispatchTouchEvent(event)) {
	    	if (event.getAction() == MotionEvent.ACTION_UP) {
		    	if (getScreenOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
				} else if (getScreenOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
				} else if (getScreenOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				} else if (getScreenOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				}
	    	} else {
	    		return false;
	    	}
    	}
    	return true;
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // save state of your activity to outState
    }

    private int[] filterConfigSpec(int[] configSpec) {
        /* We know none of the subclasses define EGL_RENDERABLE_TYPE.
         * And we know the configSpec is well formed.
         */
        int len = configSpec.length;
        int[] newConfigSpec = new int[len + 2];
        System.arraycopy(configSpec, 0, newConfigSpec, 0, len-1);
        newConfigSpec[len-1] = EGL10.EGL_RENDERABLE_TYPE;
        newConfigSpec[len] = 4; /* EGL_OPENGL_ES2_BIT */
        newConfigSpec[len+1] = EGL10.EGL_NONE;
        return newConfigSpec;
    }
    
    private int[] mValue = new int[16];

    private int findConfigAttrib(EGL10 egl, EGLDisplay display,
            EGLConfig config, int attribute, int defaultValue) {

        if (egl.eglGetConfigAttrib(display, config, attribute, mValue)) {
            return mValue[0];
        }
        return defaultValue;
    }
    public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display,
            EGLConfig[] configs) {
        EGLConfig closestConfig = null;
        int closestDistance = 1000;
        for(EGLConfig config : configs) {
            int d = findConfigAttrib(egl, display, config,
                    EGL10.EGL_DEPTH_SIZE, 0);
            int s = findConfigAttrib(egl, display, config,
                    EGL10.EGL_STENCIL_SIZE, 0);
            if (d >= 0 && s>= 0) {
                int r = findConfigAttrib(egl, display, config,
                        EGL10.EGL_RED_SIZE, 0);
                int g = findConfigAttrib(egl, display, config,
                         EGL10.EGL_GREEN_SIZE, 0);
                int b = findConfigAttrib(egl, display, config,
                          EGL10.EGL_BLUE_SIZE, 0);
                int a = findConfigAttrib(egl, display, config,
                        EGL10.EGL_ALPHA_SIZE, 0);
                int distance = Math.abs(r - 8)
                            + Math.abs(g - 8)
                            + Math.abs(b - 8)
                            + Math.abs(a - 0);
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestConfig = config;
                }
            }
        }
        return closestConfig;
    }
    
    public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
    	int[] mConfigSpec = mConfigSpec = filterConfigSpec(new int[] {
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 0,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE});
    	
        int[] num_config = new int[1];
        if (!egl.eglChooseConfig(display, mConfigSpec, null, 0,
                num_config)) {
            throw new IllegalArgumentException("eglChooseConfig failed");
        }

        int numConfigs = num_config[0];

        if (numConfigs <= 0) {
            throw new IllegalArgumentException(
                    "No configs match configSpec");
        }

        EGLConfig[] configs = new EGLConfig[numConfigs];
        if (!egl.eglChooseConfig(display, mConfigSpec, configs, numConfigs,
                num_config)) {
            throw new IllegalArgumentException("eglChooseConfig#2 failed");
        }
        EGLConfig config = chooseConfig(egl, display, configs);
        if (config == null) {
            throw new IllegalArgumentException("No config chosen");
        }
        return config;
    }
    
    EGLSurface surface = null;
    
    public void surfaceCreated(SurfaceHolder holder) {
    	EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        // Initialize EGL for the display
        int[] version = new int[2];
        egl.eglInitialize(display, version); 
        EGLConfig config = this.chooseConfig(egl, display);
        EGLContext context = egl.eglCreateContext(display, config,
                EGL10.EGL_NO_CONTEXT, null);
        surface = egl.eglCreateWindowSurface(display, config, holder, null);
        egl.eglMakeCurrent(display, surface, surface, context);
        int hash = surface.hashCode();
        
        //IntBuffer buf = null;
        int formats[] = new int[64];
        GLES20.glGetIntegerv(GLES20.GL_COMPRESSED_TEXTURE_FORMATS, formats, 0);
        
        // HDV: Rec. 709 standard
        
        // was GL_APPLE_ycbcr_422 (ITU-R BT.601 standard)
        // becomes GL_APPLE_rgb_422: In shaders, mapped as: Y -> G, Cb -> B and Cr -> R
        // 
        
        // iOS7 iPhone 5S:
        // GL_OES_depth_texture GL_OES_depth24 GL_OES_element_index_uint GL_OES_fbo_render_mipmap
        // GL_OES_mapbuffer GL_OES_packed_depth_stencil GL_OES_rgb8_rgba8 GL_OES_standard_derivatives
        // GL_OES_texture_float GL_OES_texture_half_float GL_OES_texture_half_float_linear
        // GL_OES_vertex_array_object GL_EXT_blend_minmax GL_EXT_color_buffer_half_float GL_EXT_debug_label
        // GL_EXT_debug_marker GL_EXT_discard_framebuffer GL_EXT_draw_instanced GL_EXT_instanced_arrays
        // GL_EXT_map_buffer_range GL_EXT_occlusion_query_boolean GL_EXT_pvrtc_sRGB GL_EXT_read_format_bgra
        // GL_EXT_separate_shader_objects GL_EXT_shader_framebuffer_fetch GL_EXT_shader_texture_lod
        // GL_EXT_shadow_samplers GL_EXT_sRGB GL_EXT_texture_filter_anisotropic
        // GL_EXT_texture_rg
        // GL_EXT_texture_storage GL_APPLE_copy_texture_levels GL_APPLE_framebuffer_multisample GL_APPLE_rgb_422
        // GL_APPLE_sync GL_APPLE_texture_format_BGRA8888 GL_APPLE_texture_max_level GL_IMG_read_format
        // GL_IMG_texture_compression_pvrtc

        // Common Android:
        // GL_EXT_texture_format_BGRA8888
        // GL_OES_compressed_ETC1_RGB8_texture
        // GL_OES_compressed_paletted_texture
        // GL_OES_depth_texture
        // GL_OES_draw_texture -> GL_OES_fixed_point
        // GL_OES_EGL_image
        // GL_OES_EGL_image_external
        // GL_OES_framebuffer_object
        // GL_OES_matrix_palette
        // GL_OES_packed_depth_stencil
        // GL_OES_point_size_array
        // GL_OES_point_sprite
        // GL_OES_read_format
        // GL_OES_rgb8_rgba8
        // GL_OES_texture_cube_map
        // GL_OES_texture_npot
        
        // 2.3.4 only: GL_OES_texture_float GL_OES_texture_half_float GL_OES_texture_half_float_linear
        
        // 4.2 only:
        // GL_OES_byte_coordinates GL_OES_fixed_point GL_OES_single_precision 
        // GL_OES_EGL_sync
        String glExts = GLES20.glGetString(GLES20.GL_EXTENSIONS);
        
        // 2.3.4:
        // EGL_KHR_image EGL_KHR_image_base EGL_KHR_image_pixmap EGL_ANDROID_image_native_buffer
        // EGL_ANDROID_get_render_buffer
        
        // 4.2:
        // EGL_KHR_image EGL_KHR_image_base EGL_KHR_image_pixmap EGL_ANDROID_image_native_buffer
        // EGL_KHR_gl_renderbuffer_image
        
        // EGL_KHR_gl_texture_2D_image
        // EGL_KHR_gl_texture_cubemap_image
        // EGL_KHR_fence_sync
        String eglExts = egl.eglQueryString(display, EGL10.EGL_EXTENSIONS);
        
        Log.i("GL", glExts);
        Log.i("EGL", eglExts);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    	Surface surf = holder.getSurface();
    	boolean valid = surf.isValid();
    	EGL10 egl = (EGL10) EGLContext.getEGL();
    	EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
    	egl.eglSwapBuffers(display, surface);
    }

    public void surfaceRedrawNeeded(SurfaceHolder holder) {
    	Rect frame = holder.getSurfaceFrame();
    	EGL10 egl = (EGL10) EGLContext.getEGL();
    	EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
    	egl.eglSwapBuffers(display, surface);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    // On applications creation
    @Override protected void onCreate( Bundle savedInstanceState ) 
    {
        super.onCreate( savedInstanceState );
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
     // Pass the asset manager to the native code
        sAssetManager = getAssets();
        
        ParticlesLib.createAssetManager( sAssetManager );
        
        Window win = getWindow();
        win.setGravity(Gravity.TOP);
        getWindow().getDecorView().getRootView().setFocusableInTouchMode(true);

        //setListenerToRootView();

        int addFlags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
        	| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
        	| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        	// | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR // report inset rectangle???
        	
        	//| WindowManager.LayoutParams.FLAG_SCALED
        	//| WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER
        	//| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
        	| WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        	
        	// | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS // 19+
        	| WindowManager.LayoutParams.FLAG_FULLSCREEN
        	//| WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
        	;
        
        int removeFlags = 0;//WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON;
        
        int changeFlags = addFlags | removeFlags; 
        
        win.setFlags(addFlags, changeFlags);
        win.takeSurface(this);
    }
    
    private int getScreenOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
            (rotation == Surface.ROTATION_90
                || rotation == Surface.ROTATION_270) && width > height) {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    //Log.e(TAG, "Unknown screen orientation. Defaulting to " +
                      //      "portrait.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;              
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                   // Log.e(TAG, "Unknown screen orientation. Defaulting to " +
                    //        "landscape.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;              
            }
        }

        return orientation;
    }

    @Override public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	//getWindow().takeKeyEvents(hasFocus);
    	
    	getWindow().getDecorView().getRootView().setFilterTouchesWhenObscured(true);
    	View focused = getWindow().getDecorView().getRootView().findFocus();
    }
    
    
    @Override protected void onPause() 
    {
        super.onPause();
    }

    @Override protected void onResume() 
    {
        super.onResume();
    }
    
    public static void main(String[] args) {
    	Log.i("ACTIVITY LOADED", args.toString());
        System.loadLibrary("particles");
    }
    
    boolean isOpened = false;

    public void setListenerToRootView() {
       View activityRootView = getWindow().getDecorView().getRootView();//.findViewById(android.R.id.content);
       
       activityRootView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
    	    @Override
    	    public void onFocusChange(View v, boolean hasFocus) {
    	        if (hasFocus) {
    	           ;//dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    	        }
    	    }
    	});
       
       activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
           @Override
           public void onGlobalLayout() {
        	   
        	   
/*
               int heightDiff = activityRootView.getHeight() - activityRootView.getHeight();
               if (heightDiff > 100 ) { // 99% of the time the height diff will be due to a keyboard.
                   Toast.makeText(getApplicationContext(), "Gotcha!!! softKeyboardup", 0).show();

                   if(isOpened == false){
                       //Do two things, make the view top visible and the editText smaller
                   }
                   isOpened = true;
               }else if(isOpened == true){
                   Toast.makeText(getApplicationContext(), "softkeyborad Down!!!", 0).show();                  
                   isOpened = false;
               }
               */
            }
       });
   }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	return true;
    }
    
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
    	return true;
    }
    
    @Override
    public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
    	return true;
    }
    
    boolean active = false;
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
    	if (keyCode == 82) {
    		InputMethodManager imm = (InputMethodManager)this.getSystemService(Service.INPUT_METHOD_SERVICE);
    		//imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    		//imm.showSoftInputFromInputMethod(getWindow().getDecorView().getRootView().getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
    		
    		//if (hasFocus) {
    		if (!active) {
    			active = true;
    			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    		} else {
    			active = false;
    			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    		}

            //}
    		
    		/*
    		
    		if (imm.isActive()) {
    			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    		} else {
    			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    		}*/
    		
    	} else if(keyCode == 4) {
    		// Soft exit (process exist)
    		// finish();
    		
    		// Hard exit
    		moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
    	}
    	
    	return true;
    }
    
}
