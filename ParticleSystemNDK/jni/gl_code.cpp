/* Copyright (c) <2012>, Intel Corporation
 *
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Intel Corporation nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
#include <jni.h>
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>
#define LOGV(TAG,...) __android_log_print(ANDROID_LOG_VERBOSE, TAG,__VA_ARGS__)
#define LOGD(TAG,...) __android_log_print(ANDROID_LOG_DEBUG  , TAG,__VA_ARGS__)
#define LOGI(TAG,...) __android_log_print(ANDROID_LOG_INFO   , TAG,__VA_ARGS__)
#define LOGW(TAG,...) __android_log_print(ANDROID_LOG_WARN   , TAG,__VA_ARGS__)
#define LOGE(TAG,...) __android_log_print(ANDROID_LOG_ERROR  , TAG,__VA_ARGS__)

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "util/opengl.h"

extern "C" 
{
    #include "App.h"

    // Forward declare set asset manager.
    void SetAssetManager( AAssetManager* pManager );

    JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_init( JNIEnv * env, jobject obj,  jint width, jint height,
    		jint display, jint surface);
    JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_step( JNIEnv * env, jobject obj, jfloat fElapsedTime );
    JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_createAssetManager( JNIEnv* env, jobject obj, jobject assetManager );

    JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved);
}

bool setupGraphics( int w, int h, void* display, void* surface )
{
    // Init the app
    Init( w, h, display, surface );

    return true;
}

void renderFrame( float fElapsedTime ) 
{
    // Update the app
	for(int frame = 0; frame < 25 * 5; ++frame) {
		Update( fElapsedTime );

		// Render a frame from the app
		Render();
        //                              smmmuuunnn
		nanosleep((struct timespec[]){{0, 34000000}}, NULL);
	}
}

int a = (LOGI("particles.lib", "Particles.lib loaded."), 5);

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
	LOGI("particles.lib", "Particles.lib JNI_OnLoad() called.");

	// Android does not suspend threads executing native code!!!
	// NOTE: calling AttachCurrentThread from ANY (!!!) thread
	// allows it to call ANY (!!!) Java object DIRECTLY!
	// It creates java.lang.Thread object added to "main" ThreadGroup.
	// Threads attached through JNI must call DetachCurrentThread before they exit.

	// We are in the context of class loader if we call FindClass().

	// Register all native methods (static)

    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    // Get jclass with env->FindClass.
    // Register methods with env->RegisterNatives.

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_init( JNIEnv * env, jobject obj, jint width, jint height,
		jint display, jint surface)
{
	/*
	// javax.microedition.khronos.egl.EGLDisplay display,
	// javax.microedition.khronos.egl.EGLSurface
	jclass clazz = env->GetObjectClass(display);
	jfieldID f = env->GetFieldID( clazz, "mEGLDisplay", "Lcom/google/android/gles_jni/EGLDisplayImpl;" );

	//Env->GetFieldID(ClassMYView, "five", "I");
	int i = env->GetIntField(display, f);
*/
    setupGraphics( width, height, (void*)display, (void*)surface);
}

JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_step( JNIEnv * env, jobject obj, jfloat fElapsedTime )
{
    renderFrame( fElapsedTime );
}

JNIEXPORT void JNICALL Java_com_android_particles_ParticlesLib_createAssetManager( JNIEnv* env, jobject obj, jobject assetManager )
{
    AAssetManager* mgr = AAssetManager_fromJava( env, assetManager );
    assert( mgr );
    
    // Store the assest manager for future use.
    SetAssetManager( mgr );
}

