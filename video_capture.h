#pragma once

typedef void OnCameraFrameHandler(void* frame, void* context);

struct ICamera {
    virtual void on_frame(OnCameraFrameHandler* handler, void* context) = 0;
    
    // Enable camera capture with current parameters. Dataflow may absent if camera is busy or access denied.
    virtual void on() = 0;

    // Disable camera capture. Dataflow interrupted immediately.
    virtual void off() = 0;
};

ICamera& get_camera(const char* name);
