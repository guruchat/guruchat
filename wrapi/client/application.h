#pragma once

namespace wrapi {
    class IApplicationLoadSubsystem;
    
    class IApplicationEventsHandler {
    public:
        // The first call when app is loaded. loadReasonHint
        //virtual void onLoaded(ELoadReason loadReasonHint) = 0;
    };
}

// First "entry" point that your application should implement and use for initialization etc.
bool on_application_started(wrapi::IApplicationLoadSubsystem* app);


