#pragma once

namespace wrapi {

    class ITimer {
    public:
        // Cancel wait callback, for ex. in shutdown case.
        virtual void cancel() = 0;
        
        // Repeat the timeout.
        virtual void repeat() = 0;
        
        // Available only when timed out!
        typedef void FnType_onTimedOut(void* userArg, ITimer* timer);
        virtual void relink(FnType_onTimedOut* onTimeOutHandler, void* userArg) = 0;
    };
    
    class ITimeServices {
    public:
        // Get precise nanoseconds timestamp
        virtual unsigned long long getNanos() = 0;
        
        // Approximate wait. Depends on current system hi-res timers.
        virtual void sleep(unsigned long long microseconds) = 0;
        
        typedef void FnType_onTimedOut(void* userArg, ITimer* timer);
        // Subscribe for approximate timeout. Please, use getNanos() method to query precise time.
        virtual ITimer* requestTimeout(unsigned long long microseconds,
                                       FnType_onTimedOut* onTimeOutHandler, void* userArg) = 0;
    };
}