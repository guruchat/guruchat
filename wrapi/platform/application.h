#pragma once

namespace wrapi {

    
class IApplicationLoadSubsystem {
public:
    // Load reason
    enum ELoadReason {
        LOADREASON_UNDEFINED = 2,
        // Open application
        LOADREASON_USER,
        // Run background service / remote first-load activation
        LOADREASON_AUTO,
        // Url or associated file format handler activation
        LOADREASON_URL
    };
    
    // Load reason can be used to hint startup activity scheduling
    // - essentially, to quickly load app when user opens the app, or url-click activation.
    virtual ELoadReason getLoadReason() = 0;
    
    typedef void FnType_onExitHandler(void);
    virtual void subscribeForExitEvent(FnType_onExitHandler* onExitHandler) = 0;
    
    //virtual void exit(void) = 0;
    
    virtual void log(const char16_t* message) = 0;
    
    //virtual bool queryService(const char* name, void** result) = 0;
    
    // Request access to ITimeServices
    virtual class ITimeServices* getTimeServices(void) = 0;
    
    //class IThrea
};

    
}