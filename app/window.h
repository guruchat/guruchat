//
//  window.h
//  guruchat
//
//  Created by Ivan Borisenko on 10/19/14.
//  Copyright (c) 2014 guruchat.org. All rights reserved.
//

/*
 Нам нужно, чтобы на всех устройствах была абстракция окна-холста.
 Желательно сделать
 */

#pragma once

class IWindow
{
public:
    virtual bool PutPixels();
};