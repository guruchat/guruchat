#include "app.h"

#include "video_capture.h"

void on_frontal_camera_frame(void* frame, void* context)
{
    
}

bool on_load()
{
    ICamera& cam = get_camera("frontal camera");
    cam.on_frame(on_frontal_camera_frame, 0);
    cam.on();
    
    return true;
}